// @flow

import { createActions, handleActions } from 'redux-actions'

import { Statuses } from '../../constants'
import type { Action, IDScanDocument } from '../../types/'

import initialState from './model'

export type PutDocument = (document: IDScanDocument) => Action

export type AuthActions = {
  // API
  createCustomer: () => Action,
  createCustomerFailure: () => Action,
  createCustomerSuccess: () => Action,

  scanDocuments: () => Action,
  scanDocumentsFailure: () => Action,
  scanDocumentsSuccess: (scannedData: Object) => Action,

  putDocumentOld: PutDocument,
  setupWebcam: (side: ?string) => Action,
}

export const authActions: AuthActions = createActions(
  'CREATE_CUSTOMER',
  'CREATE_CUSTOMER_FAILURE',
  'CREATE_CUSTOMER_SUCCESS',

  'SCAN_DOCUMENTS',
  'SCAN_DOCUMENTS_FAILURE',
  'SCAN_DOCUMENTS_SUCCESS',

  'PUT_DOCUMENT_OLD',
  'SETUP_WEBCAM'
)

// prettier-ignore
export default handleActions(
  {
    CREATE_CUSTOMER: state =>
      state
        .setIn(['createCustomerStatus'], Statuses.PENDING),
    CREATE_CUSTOMER_FAILURE: state =>
      state
        .setIn(['createCustomerStatus'], Statuses.FAILURE),
    CREATE_CUSTOMER_SUCCESS: state =>
      state
        .setIn(['createCustomerStatus'], Statuses.SUCCESS),
    
    SCAN_DOCUMENTS: state =>
      state
        .setIn(['scanDocumentsStatus'], Statuses.PENDING),
    SCAN_DOCUMENTS_FAILURE: state =>
      state
        .setIn(['scanDocumentsStatus'], Statuses.FAILURE),
    SCAN_DOCUMENTS_SUCCESS: (state, action) =>
      state
        .setIn(['scanDocumentsStatus'], Statuses.SUCCESS)
        .setIn(['scannedData'], action.payload),

    PUT_DOCUMENT_OLD: (state, action) =>
      state
        .setIn(['documents', action.payload.page], action.payload),
    SETUP_WEBCAM: (state, action) =>
      state
        .setIn(['webcamSideToScan'], action.payload)
  },
  initialState
)

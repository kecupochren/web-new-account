// @flow

import Immutable from 'seamless-immutable'

import { Statuses } from '../../constants/'
import type { Status } from '../../types/'

export type AuthState = {
  createCustomerStatus: Status,
  documents: {
    front: ?Object,
    back: ?Object,
  },
  scanDocumentsStatus: Status,
  scannedData: ?Object,
  webcamSideToScan: 'front' | 'back' | null,
}

const state: AuthState = {
  createCustomerStatus: Statuses.DEFAULT,
  documents: {
    front: null,
    back: null,
  },
  scanDocumentsStatus: Statuses.DEFAULT,
  scannedData: null,
  webcamSideToScan: null,
}

export default Immutable(state)

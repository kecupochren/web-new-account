// @flow

import type { State } from '../../types'

export const getApiEnabled = (state: State) => state.devMenu.enableAPI

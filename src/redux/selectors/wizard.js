// @flow

import { WizardFlow } from '../../constants/'
import type { State } from '../../types/'

export const getCurrentWizardStep = (state: State) => {
  const { activeStep } = state.wizard

  // Return static step definition so we can use next()/prev()
  return WizardFlow[activeStep]
}

export const getCurrentWizardStepName = (state: State) => {
  const { activeStep, steps } = state.wizard

  return steps[activeStep].name
}

export const getPrevWizardStep = (state: State) => {
  const { steps, options } = state.wizard
  const currentStep = getCurrentWizardStep(state)
  const prevStep = currentStep.prev(options)

  if (currentStep.prev) {
    return steps[prevStep]
  }

  return null
}

export const getNextWizardStep = (state: State) => {
  const { steps, options } = state.wizard
  const currentStep = getCurrentWizardStep(state)
  const nextStep = currentStep.next(options)

  if (nextStep) {
    return steps[nextStep]
  }

  return null
}

export const getScannedData = (state: State) => state.auth.scannedData

export const getActiveWizardSection = (state: State) => {
  const currentStep = getCurrentWizardStep(state)

  return currentStep.section
}

export const getSecondIdDocument = (state: State) => state.wizard.options.secondIDDocument

export const getDocumentsState = (state: State) => state.documents

// @flow
import type { State } from '../../types'

export const getName = (state: State) => state.customer.name
export const getGreeting = (state: State) => state.customer.greeting

import * as selectors from './customer'

describe('customer selectors', () => {
  it('can select name', () => {
    expect(selectors.getName({ customer: { name: 'Tapeface' }})).toEqual('Tapeface')
  })

  it('can select greeting', () => {
    expect(selectors.getGreeting({ customer: { greeting: 'Wazzup' }})).toEqual('Wazzup')
  })
})

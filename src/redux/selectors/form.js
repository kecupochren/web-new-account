// @flow

import type { State } from '../../types'

export const createCustomerPayload = (state: State) => {
  const { firstName, lastName, email, phone } = state.form.wizard.values

  return {
    firstName,
    lastName,
    email,
    phonePrefix: '+420',
    phoneNumber: phone,
  }
}

export const getDocuments = (state: State) => state.documents.documents.NATIONAL_ID

export const getFormValues = (state: State) => state.form.wizard.values

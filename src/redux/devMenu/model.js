// @flow

import Immutable from 'seamless-immutable'

const getFromStorage = key => {
  const item = window && window.localStorage && window.localStorage.getItem(key)

  if (item) {
    return JSON.parse(item)
  }

  return true
}

export type DevMenuState = {
  enableAccessCheck: boolean,
  enableAPI: boolean,
  enablePersistentState: boolean,
}

const state: DevMenuState = {
  enableAccessCheck: getFromStorage('enableAccessCheck'),
  enableAPI: getFromStorage('enableAPI'),
  enablePersistentState: getFromStorage('enablePersistentState'),
}

export default Immutable(state)

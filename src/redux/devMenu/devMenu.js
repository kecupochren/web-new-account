// @flow

import { createActions, handleActions } from 'redux-actions'

import type { Action } from '../../types/'

import initialState from './model'

export type DevMenuActions = {
  toggleAccessCheck: () => Action,
  toggleApi: () => Action,
  togglePersistentState: () => Action,
}

export const devMenuActions: DevMenuActions = createActions(
  'TOGGLE_ACCESS_CHECK',
  'TOGGLE_API',
  'TOGGLE_PERSISTENT_STATE'
)

// prettier-ignore
export default handleActions(
  {
    TOGGLE_ACCESS_CHECK: state => {
      window.localStorage.setItem('enableAccessCheck', !state.enableAccessCheck)
      
      return state.setIn(['enableAccessCheck'], !state.enableAccessCheck)
    },
    TOGGLE_API: state => {
      window.localStorage.setItem('enableAPI', !state.enableAPI)
      
      return state.setIn(['enableAPI'], !state.enableAPI)
    },
    TOGGLE_PERSISTENT_STATE: state => {
      window.localStorage.setItem('enablePersistentState', !state.enablePersistentState)
      
      return state.setIn(['enablePersistentState'], !state.enablePersistentState)
    }
  },
  initialState
)

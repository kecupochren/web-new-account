// @flow

import Immutable from 'seamless-immutable'

import { Documents, WizardFlow, WizardSteps } from '../../constants'

/**
 * Options
 */
export type WizardOptions = {
  isMobile: boolean,
  samePostalAddress: boolean,
  secondIDDocument: string,
  isDev: boolean,
  hasLoadedProgress: boolean,
  // does user want to continue on mobile after documents upload?
  continueOnMobile: boolean,
}

const options: WizardOptions = {
  isMobile: false,
  samePostalAddress: true,
  secondIDDocument: Documents.DRIVING_LICENCE,
  isDev: process.env.REACT_APP_ENV !== 'production',
  hasLoadedProgress: false,
  continueOnMobile: false,
}

/**
 * Steps
 */
export type WizardStep = {
  url: string,
  next: (options: WizardOptions) => string,
  prev: (options: WizardOptions) => string,
  section: string,

  name: string,
  valid: boolean,
  submitting: boolean,
  completed: boolean,
}

const steps = {}

Object.keys(WizardFlow).forEach(step => {
  steps[step] = {
    ...WizardFlow[step],
    name: step,
    submitting: false,
    valid: true,
    completed: false,
  }
})

/**
 * Model
 */
export type WizardStepsState = { [name: string]: WizardStep }

export type WizardState = {
  activeStep: string,
  steps: WizardStepsState,
  options: WizardOptions,
}

const state: WizardState = {
  activeStep: WizardSteps.INTRO,
  steps,
  options,
}

export default Immutable(state)

// @flow

import reducer, { wizardActions as actions } from './'
import initialState, { type WizardStepsState } from './model'

declare var describe: any
declare var expect: any
declare var it: any

const testSteps: WizardStepsState = {
  FOO: {
    url: 'FOO',
    next: () => 'BAR',
    prev: () => '',
    section: 'FOO-Section',
    name: 'FOO',
    valid: true,
    submitting: false,
    completed: false,
  },
  BAR: {
    url: 'BAR',
    next: () => 'BAZ',
    prev: () => 'FOO',
    section: 'BAR-Section',
    name: 'BAR',
    valid: true,
    submitting: false,
    completed: false,
  },
  BAZ: {
    url: 'BAZ',
    next: () => 'BAX',
    prev: () => 'BAR',
    section: 'BAZ-Section',
    name: 'BAZ',
    valid: true,
    submitting: false,
    completed: false,
  },
}

const testState = initialState.merge({
  steps: testSteps,
  activeStep: 'FOO',
})

describe('wizard reducer', () => {
  describe('HANDLE_SUBMIT_STEP', () => {
    it('sets step submitting to true', () => {
      const step = 'FOO'
      const state = reducer(testState, actions.handleSubmitStep(step))

      expect(state.steps[step].submitting).toBe(true)
    })
  })

  // describe('HANDLE_SUBMIT_STEP_SUCCESS', () => {
  //   it('completes this step and activates next one', () => {
  //     const thisStep = 'FOO'
  //     const nextStep = 'BAR'
  //     const state = reducer(testState, actions.handleSubmitStepSuccess(thisStep))

  //     expect(state.steps[thisStep].completed).toBe(true)
  //     expect(state.steps[thisStep].submitting).toBe(false)
  //     expect(state.activeStep).toBe(nextStep)
  //   })
  // })

  // describe('SET_ACTIVE_STEP', () => {
  //   it('sets activeStep, sets all steps completed up to this one', () => {
  //     const step = 'BAZ'
  //     const state = reducer(testState, actions.setActiveStep(step))

  //     expect(state.activeStep).toBe(step)
  //     expect(state.steps.FOO.completed).toBe(true)
  //     expect(state.steps.BAR.completed).toBe(true)
  //   })
  // })

  describe('SET_USE_WEBCAM', () => {
    it('sets useWebcam option flag', () => {
      const state = reducer(testState, actions.setUseWebcam(true))

      expect(state.options.useWebcam).toBe(true)
    })
  })

  describe('SET_SECOND_ID_DOCUMENT', () => {
    it('sets second identification document', () => {
      const doc = 'foo'
      const state = reducer(testState, actions.setSecondIdDocument(doc))

      expect(state.options.secondIDDocument).toBe(doc)
    })
  })

  describe('SET_SAME_POSTAL_ADDRESS', () => {
    it('sets samePostalAddress bool', () => {
      const state = reducer(testState, actions.setSamePostalAddress(false))

      expect(state.options.samePostalAddress).toBe(false)
    })
  })

  describe('SET_IS_MOBILE', () => {
    it('sets isMobile bool', () => {
      const state = reducer(testState, actions.setIsMobile(true))

      expect(state.options.isMobile).toBe(true)
    })
  })
})

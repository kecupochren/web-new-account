// @flow

import Immutable from 'seamless-immutable'
import { createActions, handleActions } from 'redux-actions'

import { WizardFlow } from '../../constants/'
import type { Action } from '../../types/'

import initialState from './model'

export type WizardActions = {
  loadProgress: () => Action,
  loadProgressSuccess: () => Action,
  handleSubmitStep: (step: string) => Action,
  handleSubmitStepSuccess: (step: string) => Action,
  setActiveStep: (step: string) => Action,
  setUseWebcam: (bool: boolean) => Action,
  setSecondIdDocument: (document: string) => Action,
  setSamePostalAddress: (isSame: boolean) => Action,
  setIsMobile: (isMobile: boolean) => Action,
  setContinueOnMobile: (continueOnMobile: boolean) => Action,
}

export const wizardActions: WizardActions = createActions(
  'LOAD_PROGRESS',
  'LOAD_PROGRESS_SUCCESS',
  'HANDLE_SUBMIT_STEP',
  'HANDLE_SUBMIT_STEP_FAILURE',
  'HANDLE_SUBMIT_STEP_SUCCESS',
  'SET_ACTIVE_STEP',
  'SET_USE_WEBCAM',
  'SET_SECOND_ID_DOCUMENT',
  'SET_SAME_POSTAL_ADDRESS',
  'SET_IS_MOBILE',
  'SET_CONTINUE_ON_MOBILE'
)

// prettier-ignore
export default handleActions(
  {
    LOAD_PROGRESS_SUCCESS: state =>
      state.setIn(['options', 'hasLoadedProgress'], true),
    HANDLE_SUBMIT_STEP: (state, action) =>
      state.setIn(['steps', action.payload, 'submitting'], true),
    HANDLE_SUBMIT_STEP_SUCCESS: (state, action) => {
      const thisStep = action.payload
      const nextStep = WizardFlow[thisStep].next(state.options)
      
      return state
        .setIn(['activeStep'], nextStep)
        .setIn(['steps', thisStep, 'submitting'], false)
        .setIn(['steps', thisStep, 'completed'], true)
    },
    
    SET_ACTIVE_STEP: (state, action) => {
      const step = action.payload      
      let newState = Immutable(state)
      let prevStep = WizardFlow[step] && WizardFlow[step].prev(state.options)

      // Set all steps completed up to this one
      while (prevStep) {
        newState = newState.setIn(['steps', prevStep, 'completed'], true)
        prevStep = WizardFlow[prevStep].prev(state.options)
      }

      // Set active step
      return newState.setIn(['activeStep'], step)    
    },
    
    SET_USE_WEBCAM: (state, action) =>
      state.setIn(['options', 'useWebcam'], action.payload),
    
    SET_SECOND_ID_DOCUMENT: (state, action) =>
      state.setIn(['options', 'secondIDDocument'], action.payload),

    SET_SAME_POSTAL_ADDRESS: (state, action) =>
      state.setIn(['options', 'samePostalAddress'], action.payload),
    
    SET_IS_MOBILE: (state, action) =>
      state.setIn(['options', 'isMobile'], action.payload),

    SET_CONTINUE_ON_MOBILE: (state, action) =>
      state.setIn(['options', 'continueOnMobile'], action.payload)
  },
  initialState
)

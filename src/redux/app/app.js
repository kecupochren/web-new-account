// @flow

import { createActions, handleActions } from 'redux-actions'

import type { Action } from '../../types/'

import initialState from './model'

export type OpenModalAction = (modalId: ?string) => Action

export type AppActions = {
  initialize: () => Action,
  initializeFailure: () => Action,
  initializeSuccess: () => Action,
  putValidations: (validations: Object) => Action,
  openModal: OpenModalAction,
}

export const appActions: AppActions = createActions(
  'INITIALIZE',
  'INITIALIZE_FAILURE',
  'INITIALIZE_SUCCESS',
  'PUT_VALIDATIONS',
  'OPEN_MODAL'
)

// prettier-ignore
export default handleActions(
  {
    INITIALIZE_SUCCESS: state =>
      state.merge({ initialized: true }),
    
    PUT_VALIDATIONS: (state, action) =>
      state.merge({ validations: action.payload }),
    
    OPEN_MODAL: (state, action) =>
      state.merge({ openModalId: action.payload }),
  },
  initialState
)

// @flow

import Immutable from 'seamless-immutable'

type Rule = {
  regexp: ?string,
  min: ?number,
  max: ?number,
}

export type APIValidations = {
  rules: { [rule: string]: Rule },
  errorMessages: Object,
}

export type AppState = {
  initialized: boolean,
  validations: APIValidations,
  openModalId: ?string,
}

const state: AppState = {
  initialized: false,
  validations: {
    rules: {},
    errorMessages: {},
  },
  openModalId: null,
}

export default Immutable(state)

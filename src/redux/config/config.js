// @flow
import { createActions, handleActions } from 'redux-actions'

import initialState from './model'
import { Environments } from '../../constants'
import { getActionTypes } from '../../utils'

export const configActions = createActions('SET_ENVIRONMENT', 'SET_LANGUAGE')

export const { setEnvironment, setLanguage } = configActions

export const ActionTypes = getActionTypes(configActions)

export default handleActions(
  {
    [setEnvironment]: (state, action) =>
      state.set('apiUrl', Environments[action.payload].apiUrl).set('environment', action.payload),

    [setLanguage]: (state, action) =>
      state.merge({
        lang: action.payload,
      }),
  },
  initialState
)

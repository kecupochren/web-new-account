// @flow

import Immutable from 'seamless-immutable'

export type DocumentsState = {
  documents: {
    NATIONAL_ID: {
      front: ?Object,
      back: ?Object,
    },
    SECOND_ID: {
      front: ?Object,
      // back side not used but we need this distinction for UI (DocumentUpload)
      back: ?Object,
    },
    SELFIE: ?Object,
  },
}

const state: DocumentsState = {
  documents: {
    NATIONAL_ID: {
      front: null,
      back: null,
    },
    SECOND_ID: {
      front: null,
      back: null,
    },
    SELFIE: null,
  },
}

export default Immutable(state)

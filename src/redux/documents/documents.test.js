import { Uploads } from '../../constants'

import reducer, { documentsActions as actions } from './'
import initialState from './model'

const { NATIONAL_ID, SECOND_ID } = Uploads

describe('documents reducer', () => {
  describe('PUT_DOCUMENT', () => {
    describe('when side is defined', () => {
      it('puts doc by uploadType and side', () => {
        const uploadType = NATIONAL_ID
        const side = 'front'
        const doc = { uploadType, side }
        const state = reducer(initialState, actions.putDocument(doc))

        expect(state.documents[uploadType][side]).toEqual(doc)
      })
    })

    describe('when side is not defined', () => {
      it('puts doc by uploadType', () => {
        const uploadType = SECOND_ID
        const doc = { uploadType }
        const state = reducer(initialState, actions.putDocument(doc))

        expect(state.documents[uploadType]).toEqual(doc)
      })
    })
  })

  describe('REMOVE_DOCUMENT', () => {
    it('removes doc by uploadType & side', () => {
      const uploadType = NATIONAL_ID
      const side = 'front'
      const doc = { uploadType, side }
      const state = reducer(initialState, actions.removeDocument(doc))

      expect(state.documents[uploadType][side]).toEqual(null)
    })
  })
})

// @flow

import Immutable from 'seamless-immutable'
import { createActions, handleActions } from 'redux-actions'

import type { Action, UploadedDocument } from '../../types/'

import initialState from './model'

export type DocumentsActions = {
  putDocument: (document: ?UploadedDocument) => Action,
  removeDocument: ({ uploadType: string, side: ?string }) => Action,
  loadState: (state: Object) => Action,
}

export const documentsActions: DocumentsActions = createActions(
  'PUT_DOCUMENT',
  'REMOVE_DOCUMENT',
  'LOAD_STATE'
)

export default handleActions(
  {
    PUT_DOCUMENT: (state, { payload }) => {
      const { uploadType, side }: UploadedDocument = payload
      const path = ['documents', uploadType]

      if (side) path.push(side)

      return state.setIn(path, payload)
    },

    REMOVE_DOCUMENT: (state, { payload }) => {
      const { uploadType, side }: UploadedDocument = payload
      const path = ['documents', uploadType]

      if (side) path.push(side)

      return state.setIn(path, null)
    },

    LOAD_STATE: (state, action) => Immutable(action.payload),
  },
  initialState
)

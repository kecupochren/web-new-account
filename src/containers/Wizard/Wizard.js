// @flow

/** How to use renderProps pattern: https://cdb.reacttraining.com/use-a-render-prop-50de598f11ce */

import React, { Component } from 'react'
import get from 'lodash.get'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { change as changeAction, reduxForm } from 'redux-form'
import bowser from 'bowser'

import { Routes, WizardSteps, WizardFlow } from '../../constants/'
import { Button, Page, Card, Stepper, Spinner } from '../../components/'
import { appActions } from '../../redux/app'
import { wizardActions as actions } from '../../redux/wizard'
import { documentsActions } from '../../redux/documents'
import { validator, createValidations } from '../../utils/validation/'
import type {
  State,
  Dispatch,
  DevMenuState,
  DocumentsActions,
  WizardOptions,
  WizardState,
  WizardActions,
  OpenModalAction,
} from '../../types'

import styles from './styles.scss'

export type WizardActionsFinal = WizardActions &
  DocumentsActions & {
    handleSubmit: () => void, // redux-form submit handler
    handleDoneScanning: (document: string) => void,
    handleSetContinueOnMobile: (continueOnMobile: boolean) => void,
    openModal: OpenModalAction,
  }

/** Props we supply to the child component */
export type WizardRenderProps = {
  submitting: boolean,
  valid: boolean,
  options: WizardOptions,
  actions: WizardActionsFinal,
  formValues: Object,
}

/** Props we get from the parent component */
type Props = {
  step: string,
  withoutStepper?: boolean,
  wide?: boolean,
  initialValues?: (state: Object) => Object,
  // Prefill values for Dev mode
  prefill?: Object,
  validate?: (
    // Util for declaring validation rules neatly
    validator: Function,
    // Server supplied validation rules transformed into functions
    validations: Object,
    // Options, for cases where the validation depends on them
    options: WizardOptions
  ) => (fields: Object) => Object,
}

/** Final props shape after connect() */
type PropsFinal = Props &
  WizardActionsFinal & {
    wizard: WizardState,
    push: (route: string) => void,
    change: (field: string, value: string) => void,
    children: (renderProps: WizardRenderProps) => any,
    spinnerMessageId: ?string,
    formValues: Object,
    devMenu: DevMenuState,
    openModal: OpenModalAction,
    // Supplied by reduxForm
    valid: boolean,
    initialize: (values?: Object) => void,
  }

type CompState = {
  progressLoaded: boolean,
}

export class Wizard extends Component<PropsFinal, CompState> {
  state = { progressLoaded: false }

  // Gets called on every Wizard step
  componentDidMount() {
    // this.loadProgress()
    this.validateProgress()
    this.updateMobileFlag()
  }

  loadProgress = () => {
    const {
      loadProgress,
      wizard: { options },
      devMenu: { enableAccessCheck, enablePersistentState },
    } = this.props

    if (!options.hasLoadedProgress && enableAccessCheck && enablePersistentState) {
      loadProgress()
    }
  }

  updateMobileFlag = () => {
    this.props.setIsMobile(bowser.mobile)
  }

  validatePrevStepCompleted = () => {
    const { step, wizard: { steps, options }, devMenu: { enableAccessCheck } } = this.props

    const prev = WizardFlow[step].prev(options)
    const prevStep = steps[prev]
    const canEnter = prevStep ? prevStep.completed : true

    if (!canEnter && enableAccessCheck) {
      this.props.push(Routes.INTRO)
    } else {
      this.props.setActiveStep(step)
    }
  }

  validateUsingMobileToUpload = () => {
    const { step, wizard: { options }, devMenu: { enableAccessCheck } } = this.props

    const stepsOnMobile = [
      WizardSteps.ID_UPLOAD_BACK,
      WizardSteps.ID_MINED_DATA_CHECK,
      WizardSteps.SECOND_ID_SELECT,
      WizardSteps.SECOND_ID_UPLOAD_FRONT,
      WizardSteps.SECOND_ID_MINED_DATA_CHECK,
    ]
    const shouldBeOnMobile = stepsOnMobile.includes(step)
    const isOnMobile = options.isMobile

    if (shouldBeOnMobile && !isOnMobile && enableAccessCheck) {
      this.props.push(Routes.ID_UPLOAD_FRONT)
    }
  }

  validateProgress = () => {
    this.validatePrevStepCompleted()
    this.validateUsingMobileToUpload()
  }

  handlePrefill = () => {
    const { change, prefill } = this.props

    if (prefill) {
      // $FlowFixMe
      Object.entries(prefill).forEach(([field, value]) => change(field, value))
    }
  }

  handleDoneScanning = () => {
    const nextStep = WizardSteps.CARD_DELIVERY_ADDRESS

    this.props.setActiveStep(nextStep)
    this.props.openModal(null)
    this.props.push(WizardFlow[nextStep].url)
  }

  handleSetContinueOnMobile = (continueOnMobile: boolean) => {
    this.props.setContinueOnMobile(continueOnMobile)
    this.props.handleSubmit()
  }

  props: PropsFinal

  render() {
    const {
      children,
      formValues = {},
      handleSubmit,
      handleSubmitStep,
      handleSubmitStepSuccess,
      loadProgress,
      loadProgressSuccess,
      loadState,
      openModal,
      prefill,
      putDocument,
      removeDocument,
      setActiveStep,
      setIsMobile,
      setSamePostalAddress,
      setSecondIdDocument,
      setUseWebcam,
      setContinueOnMobile,
      spinnerMessageId,
      step,
      valid,
      wide,
      withoutStepper,
      wizard: { steps, options },
    } = this.props

    const submitting = steps[step].submitting
    const withPrefill = Boolean(options.isDev && prefill)
    const renderProps: WizardRenderProps = {
      submitting,
      valid,
      options,
      formValues,
      actions: {
        handleDoneScanning: this.handleDoneScanning,
        handleSetContinueOnMobile: this.handleSetContinueOnMobile,
        handleSubmit,
        handleSubmitStep,
        handleSubmitStepSuccess,
        loadProgress,
        loadProgressSuccess,
        loadState,
        openModal,
        putDocument,
        removeDocument,
        setActiveStep,
        setContinueOnMobile,
        setIsMobile,
        setSamePostalAddress,
        setSecondIdDocument,
        setUseWebcam,
      },
    }

    return (
      <Page>
        {!withoutStepper && <Stepper />}

        <Card wide={wide} className={styles.form}>
          {spinnerMessageId && <Spinner isVisible={submitting} messageId={spinnerMessageId} />}

          <form onSubmit={handleSubmit}>
            {children(renderProps)}

            {withPrefill && (
              <Button anchor center onClick={this.handlePrefill}>
                Prefill
              </Button>
            )}
          </form>
        </Card>
      </Page>
    )
  }
}

const stateToProps = (state: State, { initialValues, validate, ...rest }: Props) => {
  const { app, devMenu, form, wizard } = state
  const { options } = wizard

  // Transform API validation rules into test functions
  const validations = createValidations(app.validations)

  return {
    devMenu,
    wizard,
    initialValues: typeof initialValues === 'function' ? initialValues(state) : initialValues,
    validate: validate && (values => validate(validator, validations, options)(values)),
    formValues: get(form, 'wizard.values'),
    ...rest,
  }
}

const dispatchToProps = {
  ...actions,
  ...documentsActions,
  openModal: appActions.openModal,
  change: changeAction,
  push,
}

const withStore = connect(stateToProps, dispatchToProps)

const withForm = reduxForm({
  form: 'wizard',
  onSubmit: (_, dispatch: Dispatch, { step }: Props) => dispatch(actions.handleSubmitStep(step)),
  enableReinitialize: true,
  destroyOnUnmount: false,
})

export default withStore(withForm(Wizard))

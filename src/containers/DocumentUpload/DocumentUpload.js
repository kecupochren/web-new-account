// @flow

import React, { PureComponent, Fragment } from 'react'
import { connect } from 'react-redux'
import { injectIntl } from 'react-intl'
import bowser from 'bowser'

import { Documents } from '../../constants/'
import { FileInputButton, Webcam, Button, Text } from '../../components/'
import type { State, WizardActionsFinal, WizardOptions } from '../../types/'

// For Dev mode
import prefillFront from '../../assets/img/id-front.base64'
import prefillBack from '../../assets/img/id-back.base64'
import prefillSelfie from '../../assets/img/selfie.base64'

import styles from './styles.scss'

export type UploadedDocument = {
  /** For UI */
  preview: string, // base64 WITH headers
  uploadType: string, // ID, Second ID or Selfie?
  side?: string, // ID Front or Back?

  /** For API request */
  data: string, // base64 WITHOUT headers
  page: string,
  documentType: string,
  contentType: string,
  originalFilename: string,
}

type Props = {
  uploadType: string,
  document: string,
  actions: WizardActionsFinal,
  intl: Object,
  options: WizardOptions,
  side: string,
  formValues: Object,
  uploadedDocument: UploadedDocument,
}

type CompState = {
  cameraOpen: boolean,
}

export class DocumentUpload extends PureComponent<Props, CompState> {
  state = { cameraOpen: false }

  openWebcam = () => this.setState({ cameraOpen: true })

  closeWebcam = () => this.setState({ cameraOpen: false })

  handleUpload = (data: string) => {
    const { actions, document, uploadType, side } = this.props

    const documentFinal: UploadedDocument = {
      preview: data, // keep headers to use in img src property
      uploadType,
      side,
      // For the API
      data: data.split(',')[1], // remove headers
      page: side,
      documentType: document,
      contentType: 'image/jpeg',
      originalFilename: `document-${document}-${side}`,
    }

    actions.putDocument(documentFinal)
  }

  handleRetry = () => {
    const { actions, uploadType, side } = this.props

    actions.removeDocument({ uploadType, side })

    // Reopen webcam if it's closed
    this.openWebcam()
  }

  handleContinue = () => {
    this.closeWebcam()
    this.props.actions.handleSubmit()
  }

  handleDevPrefill = () => {
    const { side } = this.props
    let image = prefillSelfie

    if (side && side === 'front') {
      image = prefillFront
    }

    if (side && side === 'back') {
      image = prefillBack
    }

    this.handleUpload(image)

    bowser.android && this.openWebcam()
  }

  props: Props

  renderIOSContent = () => (
    <FileInputButton onUpload={this.handleUpload} uploadedDocument={this.props.uploadedDocument} />
  )

  renderAndroidContent = () => {
    const { cameraOpen } = this.state
    const { uploadedDocument, document } = this.props

    const hasUploaded = Boolean(uploadedDocument)
    const preview = uploadedDocument && uploadedDocument.preview

    return (
      <Fragment>
        <div className={styles.preview}>{preview && <img alt="" src={preview} />}</div>

        <Button
          center
          wide
          anchor={hasUploaded}
          messageId={hasUploaded ? 'button.captureAgain' : 'button.capture'}
          onClick={hasUploaded ? this.handleRetry : this.openWebcam}
        />

        {cameraOpen && (
          <Webcam
            document={document}
            isMobile
            onCapture={this.handleUpload}
            onConfirm={this.handleContinue}
            onRetry={this.handleRetry}
            uploadedDocument={uploadedDocument}
          />
        )}
      </Fragment>
    )
  }

  renderDesktopContent = () => (
    <div>
      <Button className="mt-4" anchor center large type="button" messageId="button.resendSmsCode" />

      <div className={styles.image}>
        <img alt="" src={require('../../assets/img/document-scan.png')} />
      </div>

      <Button
        className="mt-4"
        center
        primary
        onClick={this.props.actions.handleDoneScanning}
        messageId="button.doneScanning"
      />

      <Button className="mt-4" anchor center large type="button" messageId="button.cantScan" />
    </div>
  )

  render() {
    const {
      document,
      formValues: { phone },
      intl,
      options: { isMobile, isDev },
      side,
      uploadedDocument,
    } = this.props

    const isSelfie = document === Documents.SELFIE
    const device = isMobile ? 'mobile' : 'desktop'
    const message = isSelfie ? 'selfie' : side
    const hasUploaded = Boolean(uploadedDocument) && bowser.mobile
    const messageValues = {
      document: intl.formatMessage({ id: `documents.${document}` }),
      phone,
    }

    return (
      <Fragment>
        <Text h2 messageId={`DocumentUpload.title.${message}`} values={messageValues} />

        <Text
          messageId={`DocumentUpload.subtitle.${document}.${device}.${message}`}
          values={messageValues}
        />

        {!bowser.mobile && this.renderDesktopContent()}

        {bowser.ios && this.renderIOSContent()}

        {bowser.android && this.renderAndroidContent()}

        {hasUploaded && <Button submit primary onClick={this.handleContinue} />}

        {isDev && (
          <Button anchor center type="button" onClick={this.handleDevPrefill}>
            Prefill
          </Button>
        )}
      </Fragment>
    )
  }
}

const withStore = connect(({ documents }: State, { uploadType, side, ...rest }: Props) => {
  const uploadTypeDocs = documents.documents[uploadType]

  return {
    uploadedDocument: side ? uploadTypeDocs[side] : uploadTypeDocs,
    ...rest,
  }
})

export default injectIntl(withStore(DocumentUpload))

// @flow

import React from 'react'

import { Modals } from '../../../../constants/'
import { Modal, Text, Button } from '../../../../components/'

type Props = {
  handleContinue: () => void,
}

export const MobileUploadModal = ({ handleContinue }: Props) => (
  <Modal id={Modals.MOBILE_UPLOAD} size="lg">
    <Text h2 noMargin messageId="UploadMethodSelect.MobileUploadModal.title" />
    <Button
      anchor
      messageId="UploadMethodSelect.MobileUploadModal.button"
      onClick={handleContinue}
    />
  </Modal>
)

export default MobileUploadModal

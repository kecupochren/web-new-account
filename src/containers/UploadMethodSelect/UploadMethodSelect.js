// @flow

/**
 * Deprecated, business dropped desktop file upload support for MVP.
 *
 * Left here because they will change their mind again...
 */

import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { injectIntl } from 'react-intl'

import { Documents, Modals } from '../../constants'
import { Button, Text } from '../../components'
import { appActions, type OpenModalAction } from '../../redux/app'
import type { WizardActionsFinal } from '../../types'

import { MobileUploadModal } from './components'
import styles from './styles.scss'

type Props = {
  actions: WizardActionsFinal,
  openModal: OpenModalAction,
  document: string,
  intl: Object,
}

export class UploadMethodSelect extends PureComponent<Props> {
  props: Props

  handleSetUseWebcam = (useWebcam: boolean) => {
    const { actions } = this.props

    actions.setUseWebcam(useWebcam)
    actions.handleSubmit()
  }

  handleSkip = () => {
    const { actions, document } = this.props

    actions.handleDoneScanning(document)
  }

  render() {
    const { document, intl, openModal }: Props = this.props

    const isSelfie = document === Documents.SELFIE
    const method = isSelfie ? 'capture' : 'upload'
    const quantity = document === Documents.NATIONAL_ID ? 'plural' : 'singular'

    const documentMessage = intl.formatMessage({ id: `documents.${document}` })
    const documentAltMessage = intl.formatMessage({ id: `documents.${document}.alt` })
    const methodMessage = intl.formatMessage({ id: `documents.uploadMethod.${method}` })
    const infoMessage = intl.formatMessage({ id: 'UploadMethodSelect.info' })

    return (
      <div>
        <Text
          h2
          noMargin
          messageId="UploadMethodSelect.title"
          values={{ document: documentMessage, method: methodMessage }}
        />

        <Text
          messageId={`UploadMethodSelect.subtitle.${document}`}
          values={{ info: infoMessage }}
        />

        <Button
          type="button"
          className={`mt-2 ${styles.btn}`}
          center
          onClick={() => openModal(Modals.MOBILE_UPLOAD)}
          messageId="button.usePhoneCam"
        />

        <MobileUploadModal handleContinue={this.handleSkip} />

        <div className={styles.divider}>
          <Text messageId="UploadMethodSelect.or" />
        </div>

        <Text center messageId="UploadMethodSelect.webcam.title" />

        <Button
          onClick={() => this.handleSetUseWebcam(true)}
          center
          className={`mt-2 ${styles.btn}`}
          messageId="UploadMethodSelect.webcam.button"
        />

        <div className={styles.divider}>
          <Text messageId="UploadMethodSelect.or" />
        </div>

        {isSelfie ? (
          <Button
            anchor
            center
            onClick={this.handleSkip}
            messageId="UploadMethodSelect.button.skip"
          />
        ) : (
          <Button
            anchor
            center
            onClick={() => this.handleSetUseWebcam(false)}
            message={{
              id: `UploadMethodSelect.upload.button.${quantity}`,
              values: { document: documentAltMessage },
            }}
          />
        )}
      </div>
    )
  }
}

const withStore = connect(null, {
  openModal: appActions.openModal,
})

export default injectIntl(withStore(UploadMethodSelect))

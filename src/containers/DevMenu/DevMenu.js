// @flow

import React, { PureComponent } from 'react'
import { push } from 'react-router-redux'
import { connect } from 'react-redux'

import { Text, Button, EnvSelect } from '../../components'
import { devMenuActions, type DevMenuActions, type DevMenuState } from '../../redux/devMenu'

import styles from './styles.scss'

type Props = DevMenuActions & {
  devMenu: DevMenuState,
  push: (route: string) => void,
}

type State = {
  isOpen: boolean,
  isEnabled: boolean,
}

class DevMenu extends PureComponent<Props, State> {
  state = {
    isOpen: false,
    isEnabled: true,
  }

  openMenu = () => this.setState({ isOpen: true })

  closeMenu = () => this.setState({ isOpen: false })

  disableMenu = () => this.setState({ isEnabled: false })

  handleReset = () => {
    window.localStorage.removeItem('moneta-onboarding')
    this.props.push('/')
    window.location.reload()
  }

  props: Props

  render() {
    const { isOpen, isEnabled } = this.state
    const {
      devMenu: { enableAccessCheck, enableAPI },
      toggleAccessCheck,
      toggleApi,
      // togglePersistentState,
    } = this.props

    return (
      isEnabled && (
        <div className={styles.wrapper}>
          {isOpen && (
            <div role="button" tabIndex="0" className={styles.overlay} onClick={this.closeMenu} />
          )}

          {isOpen && (
            <div className={styles.menu}>
              <Text noMargin>Settings:</Text>

              <Button anchor noMargin onClick={() => toggleApi()}>
                {enableAPI ? 'Disable API' : 'Enable API'}
              </Button>

              <Button anchor noMargin onClick={() => toggleAccessCheck()}>
                {enableAccessCheck ? 'Disable Step Access Check' : 'Enable Step Access Check'}
              </Button>

              {/* <Button anchor noMargin onClick={() => togglePersistentState()}>
                {enablePersistentState ? 'Disable Persistent State' : 'Enable Persistent State'}
              </Button> */}

              <Button anchor noMargin onClick={this.handleReset}>
                Reset session
              </Button>

              {/* <Text>Environment:</Text>
              <EnvSelect /> */}

              <Button anchor className="text--right" noMargin onClick={this.closeMenu}>
                Close
              </Button>
            </div>
          )}

          <Button
            anchor
            className={styles.btnToggle}
            onClick={isOpen ? this.closeMenu : this.openMenu}>
            Dev
          </Button>
        </div>
      )
    )
  }
}

const withStore = connect(({ devMenu }) => ({ devMenu }), {
  ...devMenuActions,
  push,
})

export default withStore(DevMenu)

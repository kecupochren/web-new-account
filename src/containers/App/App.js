// @flow
import React, { PureComponent } from 'react'
import { ConnectedRouter } from 'react-router-redux'
import { connect } from 'react-redux'
import { HashRouter as Router, Route, Switch } from 'react-router-dom'
import { IntlProvider, addLocaleData } from 'react-intl'
import get from 'lodash.get'
import store from 'store'
// $FlowFixMe
import cs from 'react-intl/locale-data/cs'

import { Spinner } from '../../components/'
import { appActions } from '../../redux/app'
import { configActions } from '../../redux/config'
import { history } from '../../redux'
import * as Pages from '../../pages'
import { Routes } from '../../constants'
import * as messages from '../../i18n'
import type { Action, AppState } from '../../types/'

import '../../assets/css/global.scss'

addLocaleData(cs)

type Props = {
  app: AppState,
  lang: string,
  setEnvironment: (env: string) => Action,
  initialize: () => Action,
}

// const isDev = process.env.NODE_ENV !== 'production'

export class App extends PureComponent<Props> {
  componentWillMount() {
    const env = 'DEV'

    this.props.setEnvironment(env)
  }

  componentDidMount() {
    this.props.initialize()
  }

  props: Props

  render() {
    const { app, lang } = this.props

    return (
      <IntlProvider key={lang} locale={lang} messages={messages[lang]}>
        {app.initialized ? (
          <div>
            <ConnectedRouter history={history}>
              <div>
                {/* {isDev && <EnvSelect />} */}

                <Router>
                  {
                    // prettier-ignore
                    <Switch>
                      <Route exact path={Routes.AML_QUESTIONS} component={Pages.AmlQuestions} />
                      <Route exact path={Routes.BANK_ACCOUNT_NUMBER} component={Pages.BankAccountNumber} />
                      <Route exact path={Routes.BANK_ACCOUNT_VERIFICATION} component={Pages.BankAccountVerification} />
                      <Route exact path={Routes.CARD_DELIVERY_ADDRESS} component={Pages.CardDeliveryAddress} />
                      <Route exact path={Routes.CONTINUE_ON_DESKTOP} component={Pages.ContinueOnDesktop} />
                      <Route exact path={Routes.CONTRACT} component={Pages.Contract} />
                      <Route exact path={Routes.DEVICE_TO_CONTINUE_ON_SELECT} component={Pages.DeviceToContinueOnSelect} />
                      <Route exact path={Routes.ID_MINED_DATA_CHECK} component={Pages.IDMinedDataCheck} />
                      <Route exact path={Routes.ID_UPLOAD_BACK} component={Pages.IDUploadBack} />
                      <Route exact path={Routes.ID_UPLOAD_FRONT} component={Pages.IDUploadFront} />
                      <Route exact path={Routes.INTRO} component={Pages.Intro} />
                      <Route exact path={Routes.PRE_CONTRACT_INFO} component={Pages.PreContractInfo} />
                      <Route exact path={Routes.SECOND_ID_MINED_DATA_CHECK} component={Pages.SecondIDMinedDataCheck} />
                      <Route exact path={Routes.SECOND_ID_SELECT} component={Pages.SecondIDSelect} />
                      <Route exact path={Routes.SECOND_ID_UPLOAD_FRONT} component={Pages.SecondIDUploadFront} />
                      <Route exact path={Routes.SELFIE_UPLOAD} component={Pages.SelfieUpload} />
                      <Route exact path={Routes.SMS_VERIFICATION} component={Pages.SmsVerification} />
                      <Route exact path={Routes.VICTORY} component={Pages.Victory} />
                    </Switch>
                  }
                </Router>
              </div>
            </ConnectedRouter>
          </div>
        ) : (
          <Spinner isVisible />
        )}
      </IntlProvider>
    )
  }
}

const withStore = connect(
  ({ app, config }) => ({
    app,
    lang: config.lang,
  }),
  {
    setEnvironment: configActions.setEnvironment,
    initialize: appActions.initialize,
  }
)

export default withStore(App)

import React from 'react'
import { shallow } from 'enzyme'

import { App } from './App'

describe('App', () => {
  const props = {
    app: {
      initialized: true,
    },
    initialize: jest.fn(),
    setEnvironment: jest.fn(),
  }

  it('renders without errors', () => {
    const wrapper = shallow(<App {...props} />)

    expect(wrapper).toBeDefined()
  })
})

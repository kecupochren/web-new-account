// @flow
// prettier-ignore
export default {
  'label.firstName': 'Name',
  'label.lastName': 'Surname',
  'label.phone': 'Phone',
  'label.email': 'Email',

  'button.continue': 'Continue',
  'button.upload.front': 'Upload front side',
  'button.upload.back': 'Upload back side',

  'validation.required': 'Required field',
  'validation.min_length': 'The value must be atleast {min} chars',
  'validation.invalid_email_address': 'Invalid email address',

  'app.title': 'Create account online',

  'Intro.top.title': 'Ano, jste tu správně',
  'Intro.top.subtitle': 'Svůj nový účet TOM si založíte právě zde. <br />Vše zcela online, bez nutnosti návštěvy pobočky nebe čekání na kurýra. <br /><br /> Více o účtu TOM na neniocem.cz',

  'Intro.bottom.title': 'Účet si můžete založit, pokud:',
  'Intro.bottom.rule0': '- Jste občanem ČR starším 18 let',
  'Intro.bottom.rule1': '- Máte po ruce svou občanku a řidičák nebo pas',
  'Intro.bottom.rule2': '- Už máte běžný účet u jiné banky v ČR, který jste si založil/a osobně na pobočce (číslo tohoto účtu budete potřebovat0',

  'Home.title': 'Basic info',
  'Home.subtitle': 'Svůj nový účet TOM si založíte právě zde. <br />Vše zcela online, bez nutnosti návštěvy pobočky nebe čekání na kurýra. <br /><br /> Více o účtu TOM na neniocem.cz',

  'IDScan.title': 'ID Scan',
  'IDScan.subtitle': 'Pomocí Vaší občanky si ověříme, s kým máme tu čest. Nebojte, Vaše údaje jsou u nás v naprostém bezpečí.',
}

// @flow
// prettier-ignore
export default {
  /**
   * Common
   */
  'label.firstName': 'Jméno',
  'label.lastName': 'Příjmení',
  'label.phone': 'Telefon',
  'label.email': 'Email',
  'label.birthPlace': 'Místo narození',
  'label.birthNumber': 'Rodné číslo',
  'label.birthDate': 'Datum narození',
  'label.stateOfBirth': 'Stát narození',
  'label.nationality': 'Státní občanství',
  'label.idCardValidTo': 'Platnost do (DD.MM.RRRR)',
  'label.idCardNumber': 'Číslo občanského průkazu',
  'label.street': 'Ulice',
  'label.streetNumber': 'Číslo popisné/orientační',
  'label.city': 'Město',
  'label.zip': 'PSČ',
  'label.sms': 'SMS kód',
  'label.drivingLicenceNumber': 'Číslo řidičského průkazu',
  'label.drivingLicenceValidTo': 'Platnost řidičského průkazu',

  'button.continue': 'Pokračovat',
  'button.agree': 'Souhlasím',
  'button.usePhoneCam': 'Vyfotit telefonem',
  'button.resendSmsCode': 'Pošlete mi SMS znovu',
  'button.send': 'Odeslat',
  'button.capture': 'Vyfotit',
  'button.captureAgain': 'Vyfotit znovu',
  'button.retry': 'Znovu',  
  'button.toggle': 'Přepnout',  
  'button.doneScanning': 'Mám vyfoceno',  
  'button.cantScan': 'Nemůžu/nechci fotit doklady',
  
  'button.upload.front': 'Nahrát přední stranu',
  'button.upload.front.webcam': 'Vyfotit přední stranu',
  'button.upload.front.success': 'Přední strana nahraná',
  'button.upload.back': 'Nahrát zadní stranu',
  'button.upload.back.webcam': 'Vyfotit zadní stranu',
  'button.upload.back.success': 'Zadní strana nahraná',
  'button.use': 'Použít',

  'success.smsSent': 'SMS byla odeslána',

  'error.required': 'Pole musí být vyplněno',
  'error.min_length': 'Minimální povolená délka je {min} znaků.',
  'error.max_length': 'Maximální povolená délka je {max} znaků.',
  'error.invalid_email': 'Neplatná emailová adresa',
  'error.invalid_phone': 'Bez předvolby a mezer',
  'error.invalid_birth_number': 'Neplatné rodné číslo',

  // 'error.must_be_characters_long': 'Musí být {length} znaků dlouhé',
  // 'error.invalid_address': 'Neplatná adresa',
  // 'error.invalid_zip': 'Neplatné PSČ',
  // 'error.invalid_birth_number': 'Neplatné rodné číslo',
  // 'error.invalid_email_address': 'Nevalidní e-mailová adresa',
  // 'error.invalid_phone_number': 'Nevalidní mobilní číslo',
  // 'error.must_be_a_number': 'Hodnota musí být číslo',
  // 'error.must_be_positive_number': 'Hodnota musí být kladné číslo',
  // 'error.value_must_be_less_than': 'Hodnota musí být menší než {limit}',
  // 'error.must_be_at_least': 'Hodnota musí být minimálně {min} znaků',
  // 'error.must_be_in_the_future': 'Datum musí být v budoucnosti',
  // 'error.must_be_in_the_past': 'Datum musí být v minulosti',
  // 'error.value_must_be_more_than': 'Hodnota musí být více než {limit}',
  // 'error.must_contain_at_least': 'Hodnota musí obsahovat alespoň jedno číslo',
  // 'error.must_include_only_text': 'Hodnota musí obsahovat pouze písmena',
  // 'error.must_be_no_more_than': 'Nesmí být delší než {max} znaků',
  // 'error.invalid_date': 'Nevalidní datum',
  // 'error.your_monthly_disposable_income': 'Váš měsíční dispoziční zůstatek musí být více než {diff} Kč',
  // 'error.invalid_employment_id': 'Neplatné IČO',
  // 'error.invalid_city': 'Neplatné město',
  // 'error.required_checkbox': 'Bez Vašeho souhlasu nemůžeme pokračovat',
  // 'error.must_pay_taxes_in_cr': 'Uživatel musí platit daně pouze v ČR',

  'app.title': 'Založte si účet TOM',

  'footer.title': 'Nevíte, jak dál?<br /> Pomůžeme Vám!',
  'footer.phone': '<b>Na infolince</b> <br />{phone}',
  'footer.branch': '<b>Na pobočce</b> <br />{link}',

  'documents.uploadMethod.upload': 'Nahrejte',
  'documents.uploadMethod.capture': 'Vyfoťte',

  'documents.side.front': 'přední',
  'documents.side.back': 'zadní',
  'documents.side.selfie': 'svou tvář',

  /**
   * Components
   */
  'DevMenu.enable': 'enable {feature}',
  'DevMenu.disable': 'disable {feature}',
  'DevMenu.feature.validation': 'validation',
  'DevMenu.feature.api': 'API',
  'DevMenu.feature.persistentState': 'persistent state',
  'DevMenu.reset': 'Reset session',

  /**
   * Constants
   */
  'wizardSections.WELCOME': 'Vítejte',
  'wizardSections.DOCUMENTS': 'Doklady',
  'wizardSections.DETAILS': 'Informace o Vás',
  'wizardSections.CONTRACT': 'Smlouva',
  'wizardSections.VERIFICATION': 'Ověřovací platba',

  'documents.NATIONAL_ID': 'občanský průkaz',
  'documents.NATIONAL_ID.alt': 'občanky',
  'documents.DRIVING_LICENCE': 'řidičský průkaz',
  'documents.DRIVING_LICENCE.alt': 'řidičského průkazu',
  'documents.PASSPORT': 'pas',
  'documents.PASSPORT.alt': 'pasu',
  'documents.SELFIE': 'svou tvář',
  'documents.SELFIE.alt': 'své tváře',

  /**
   * Modals
   */ 
  'ChangePhoneNumberModal.title': 'Vaše telefonní číslo',
  'ConfirmAccountModal.title': 'Pokračováním potvrzujete, že zadaný účet číslo 123456789/0000 je veden na Vaše jméno a byl založen osobně.',

  /**
   * Containers
   */
  'DocumentUpload.title.selfie': 'Vyfoťte svou tvář',
  'DocumentUpload.title.front': 'Vyfoťte svůj {document}',
  'DocumentUpload.title.back': 'Skvělé! Teď prosím vyfoťte zadní stranu',
  'DocumentUpload.subtitle.NATIONAL_ID.desktop.front': 'Pomocí Vaší občanky a druhého dokladu si ověříme, s kým máme tu čest.<br/> Na Vaše telefonní číslo {phone} jsme poslali odkaz, na kterém pohodlně vyfotíte doklady Vaším mobilem.<br/> Nebojte, Vaše údaje jsou u nás v naprostém bezpečí.',
  'DocumentUpload.subtitle.NATIONAL_ID.mobile.front': 'Pomocí Vaší občanky a druhého dokladu si ověříme, s kým máme tu čest. <br/>Nebojte, Vaše údaje jsou u nás v naprostém bezpečí.',
  'DocumentUpload.subtitle.NATIONAL_ID.mobile.back': 'I druhá strana Vaší občanky je důležitá. Postup je úplně stejný jako v předchozím kroku.',
  'DocumentUpload.subtitle.DRIVING_LICENCE.desktop.front': 'Bude to rychlovka, stačí pouze přední strana. Na Vaše telefonní číslo {phone} jsme poslali odkaz, na kterém pohodlně vyfotíte doklady Vaším mobilem. Nebojte, Vaše údaje jsou u nás v naprostém bezpečí.',
  'DocumentUpload.subtitle.DRIVING_LICENCE.mobile.front': 'Bude to rychlovka, stačí pouze přední strana. Nebojte, Vaše údaje jsou u nás v naprostém bezpečí.',
  'DocumentUpload.subtitle.PASSPORT.desktop.front': 'Bude to rychlovka, stačí pouze přední strana. Na Vaše telefonní číslo {phone} jsme poslali odkaz, na kterém pohodlně vyfotíte doklady Vaším mobilem. Nebojte, Vaše údaje jsou u nás v naprostém bezpečí.',
  'DocumentUpload.subtitle.PASSPORT.mobile.front': 'Bude to rychlovka, stačí pouze přední strana. Nebojte, Vaše údaje jsou u nás v naprostém bezpečí.',
  'DocumentUpload.subtitle.SELFIE.desktop.selfie': 'Na pobočce bychom se viděli, tak se prosím vyfoťte, abychom si byli jistí.',
  'DocumentUpload.subtitle.SELFIE.mobile.selfie': 'Na pobočce bychom se viděli, tak se prosím vyfoťte, abychom si byli jistí.',

   // Deprecated (business/risk dept. dropped desktop file upload/desktop webcam for MVP)
  'UploadMethodSelect.title': '{method} {document}',
  'UploadMethodSelect.subtitle.NATIONAL_ID': 'Pomocí Vaší občanky si ověříme, s kým máme tu čest. {info}',
  'UploadMethodSelect.subtitle.DRIVING_LICENCE': 'Bude to rychlovka, stačí pouze přední strana. {info}',
  'UploadMethodSelect.subtitle.PASSPORT': 'Bude to rychlovka, stačí pouze přední strana. {info}',
  'UploadMethodSelect.subtitle.SELFIE': 'Na pobočce bychom se viděli, tak se prosím vyfoťte, abychom si byli jistí. {info}',
  'UploadMethodSelect.info': 'Nebojte, Vaše údaje jsou u nás v naprostém bezpečí. Pošleme Vám SMSkou odkaz, na kterém pohodlně nafotíte doklady svým mobilem.',
  'UploadMethodSelect.info.mobile': 'Nebojte, Vaše údaje jsou u nás v naprostém bezpečí.',
  'UploadMethodSelect.or': 'nebo',
  'UploadMethodSelect.webcam.title': 'Můžete použít webkamerou počítače',
  'UploadMethodSelect.webcam.button': 'Vyfotit kamerou počítače',
  'UploadMethodSelect.upload.button.singular': 'Nahrajte fotku {document} z úložiště počítače',
  'UploadMethodSelect.upload.button.plural': 'Nahrajte fotky {document} z úložiště počítače',
  'UploadMethodSelect.button.skip': 'Nyní přeskočit',
  'UploadMethodSelect.MobileUploadModal.title': 'Na Vaše telefonní číslo 777 123 456 jsme poslali odkaz, na kterém pohodlně vyfotíte doklady Vaším mobilem.<br /><br /> Tuto stránku nezavírejte, po focení dokladů se sem můžete zase vrátit.',
  'UploadMethodSelect.MobileUploadModal.button': 'Mám vyfoceno',

  /**
   * Pages
   */
  'Intro.title': 'Ano, jste tu správně', 
  'Intro.subtitle': 'Svůj nový účet TOM si založíte právě zde. <br />Vše zcela online, bez nutnosti návštěvy pobočky nebe čekání na kurýra. Více o účtu TOM na <a href="http://www.neniocem.cz"/>www.neniocem.cz<a/>', 

  'SmsVerification.title': 'Vítejte v MMB, {name}! Pokračujte v založení běžného účtu Tom',
  'SmsVerification.subtitle': 'Pro ověrení Vašeho telefonního čísla zadejte SMS kód, který jsme Vám právě poslali na Vaše telefonní číslo {phone}.',
  'SmsVerification.button.changePhone': 'Změnit telefonní číslo',

  'IDMinedDataCheck.title': 'Máme vše správně?', 
  'IDMinedDataCheck.subtitle': 'Zkontrolujte prosím, zda jsou vyplněné údaje shodné s údaji na občanském průkazu. Něco nesedí? Jednoduše údaje přepište tak, jak mají být správně.', 
  'IDMinedDataCheck.address': 'Adresa', 
  'IDMinedDataCheck.spinner': 'Chvilku strpení...',  

  'SecondIDMinedDataCheck.title': 'Zkontrolujte své údaje',
  'SecondIDMinedDataCheck.subtitle': 'Opět prosím zkontrolujte, zda jsou vyplněné údaje shodné s údaji na Vašem dokladu, včetně háčků a čárek. Něco nesedí? Jednoduše údaje přepíšte tak, jak mají být správně.',

  'SecondIDSelect.title': 'Vyberte druhý doklad',
  'SecondIDSelect.subtitle': 'Známe plno zábavnejších činností než focení dokladů, ale nebojte, budete to mít raz dva. Bude to řidičák nebo pas?',

  'DeviceToContinueOnSelect.title': 'Jak budete pokračovat?',
  'DeviceToContinueOnSelect.subtitle': 'Další kroky, jako podpis smlouvy a ověřovací platbu můžete dokončit zde na mobilu nebo opět na Vašem počítači.',
  'DeviceToContinueOnSelect.btn.desktop': 'Vrátím se na počítač',
  'DeviceToContinueOnSelect.btn.mobile': 'Pokračujte na mobilu',
  
  'ContinueOnDesktop.title': 'Nyní se můžete vrátit ke svému počítači',
  'ContinueOnDesktop.subtitle': 'Na počítači na Vás čeká tlačítko “<strong>Mám dofoceno</strong>”. Po jeho stisku budete pokračovat v založení účtu. <br /><br /> Toto okno na mobilu můžete zavřít.',

  'CardDeliveryAddress.title': 'Kam Vám máme poslat platební kartu?',
  'CardDeliveryAddress.subtitle': 'Kartu Vám pošleme jako obyčejné psaní do schránky.<br /> Tuto adresu si uložíme také jako Vaši korespondenční adresu.',
  'CardDeliveryAddress.changeAddress.same': 'Jiná korespondeční adresa',
  'CardDeliveryAddress.changeAddress.not': 'Stejná korespondeční adresa',

  'AmlQuestions.title': 'Informace požadované zákonem',
  'AmlQuestions.subtitle': 'Není to kvůli naší zvědavosti. Zeptat se na tyto informace nám ukládá zákon.',
  'AmlQuestions.taxResidency.title': 'Kde platíte daň z příjmu?',
  'AmlQuestions.taxResidency.inCR': 'V České republice',
  'AmlQuestions.taxResidency.outsideCR': 'Mimo Českou republiku',
  'AmlQuestions.transactionsType.title': 'Jaké transakce budou na Vaše účtu převažovat?',
  'AmlQuestions.transactionsType.cash': 'Hotovostní',
  'AmlQuestions.transactionsType.domestic': 'Bezhotovostní tuzemské',
  'AmlQuestions.transactionsType.international': 'Bezhotovostní zahraniční',
  'AmlQuestions.incomeSource.title': 'Odkud budou peníze na Vašem účtu?',
  'AmlQuestions.incomeSource.salary': 'Příjem ze zaměstnání',
  'AmlQuestions.incomeSource.business': 'Příjem z podnikání',
  'AmlQuestions.incomeSource.pension': 'Důchod/Renta',
  'AmlQuestions.incomeSource.socialFunds': 'Sociální dávky',
  'AmlQuestions.incomeSource.gift': 'Příjem od rodičů/Výhra/Dary',

  'PreContractInfo.title': 'Projděte si prosím předsmluvní informace',
  'PreContractInfo.list.title': 'Naleznete v nich zejména informace o',
  'PreContractInfo.list.moneta': 'MONETA Money Bank',
  'PreContractInfo.list.communication': 'způsobu, jak spolu budeme komunikovat',
  'PreContractInfo.list.accountCreate': 'sjednávaném účtu',
  'PreContractInfo.list.contract': 'smlouvě samotné',
  'PreContractInfo.list.insurance': 'pojištění vkladů',
  'PreContractInfo.content.title': 'Předsmluvní informace - plné znění',
  'PreContractInfo.content.info': 'Vážíme si Vašeho zájmu o běžný účet – Tom účet! Ještě před jeho založením bychom Vám rádi poskytli pár základních informací o nás i o účtu samotném.',
  'PreContractInfo.content.bankInfo.title': 'Základní informace o bance',
  'PreContractInfo.content.bankInfo.content': '<strong>MONETA Money Bank, a.s.</strong> má sídlo v ulici <strong>Vyskočilova 1422/1a, v Praze 4 – Michle</strong>, PSČ: 140 28, IČO 256 72 720 a je zapsaná v obchodním rejstříku vedeném Městským soudem v Praze v oddíle B, vložce 5403. Působí na základě licence udělené Českou národní bankou (se sídlem v Praze 1 Na Příkopě 28, 115 03 Praha 1, IČO 48136450). Česká národní banka je pro MONETA Money Bank dohledovým orgánem.<br /><br /> Jak s námi můžete komunikovat? Můžete s námi komunikovat písemně, telefonicky nebo prostřednictvím našich poboček. Komunikovat spolu budeme v českém jazyce. V případě písemné komunikace prosím využijte poštovní adresu totožnou se sídlem MONETA Money Bank nebo poštovní adresou příslušné pobočky; další naše poštovní adresy, stejně jako elektronický a telefonický kontakt najdete na naší webové stránce <a href="https://www.moneta.cz/kontaktujte-nas/kontakt.">https://www.moneta.cz/kontaktujte-nas/kontakt.</a>',
  
  'Contract.title': 'Nyní si prosím projděte svoji smlouvu',
  'Contract.confirm': 'Souhlasím a chci podepsat',
  'Contract.reject': 'Něco nesedí',
  'Contract.sign': 'Podepsat smlouvu',
  'Contract.smsInfo': '<strong>SMS kód</strong> jsme odeslali na telefonní číslo 777 123 456',
  
  'BankAccountNumber.title': 'Ověřovací platba',
  'BankAccountNumber.subtitle': 'Pošlete si ze svého stávajícího účtu pár korun. Budeme si tak jistí, že jste to opravdu Vy.',
  'BankAccountNumber.form.title': 'Z jakého účtu si můžete převést peníze?',
  'BankAccountNumber.form.list.title': 'Z účtu, který:',
  'BankAccountNumber.form.list.yourName': 'Je veden na Vaše jméno',
  'BankAccountNumber.form.list.czechBank': 'Je veden u banky v ČR',
  'BankAccountNumber.form.list.verified': 'Byl založen osobně na pobočce',
  'BankAccountNumber.form.input': 'Číslo účtu, ze kterého pošlete platbu',
  'BankAccountNumber.form.input.note': 'Na tento účet Vám také pošleme zpět ověřovací platbu, pokud by se něco nepovedlo.',
  'BankAccountNumber.button.showPaymentInfo': 'Zobrazit platební údaje',

  'BankAccountVerification.title': 'Pošlete si z Vašeho účtu v České Spořitelně pár korun na Váš nový účet Tom.',
  'BankAccountVerification.subtitle': 'Jakmile budou peníze na účet připsány, pošleme Vám SMS a svůj nový účet můžete začít naplno využívat.',
  'BankAccountVerification.fromAccount': 'Z Vašeho účtu:',
  'BankAccountVerification.toAccount': 'Na Váš nový účet číslo:',
  'BankAccountVerification.amount': 'Částka:',
  'BankAccountVerification.dueDate': 'Nejpozději do:',
  
  'Victory.title': 'Gratulujeme!<br /> Váš nový účet TOM je založen.',
  'Victory.info': 'Na Váš e-mali jsme poslali přihlašovací ID do internetového bankovnictví.<br /><br /> Heslo do internetového bankovnictví jsme právě odeslali SMS zprávou na Vaše telefonní číslo.',
  'Victory.internetBanking.title': 'Teď můžete svůj účet začít využívat naplno v Internet bance, nebo v mobilní aplikaci Smart Banka!',
  'Victory.internetBanking.button': 'Otevřít Internet banku',
  'Victory.mobileBanking.title': 'Stáhněte si aplikaci MONETA Smart Banka',
  'Victory.mobileBanking.sms': 'Nebo vám pošleme odkaz přímo na telefon',
}

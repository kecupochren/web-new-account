// @flow
import { defineMessages } from 'react-intl'

export default defineMessages({
  greeting: {
    id: 'app.greeting.greeting',
    defaultMessage: '{greeting} {name}',
  },
  noName: {
    id: 'app.greeting.noName',
    defaultMessage: 'No name is set, try going to the root (/) path to set one via the form.',
  },
  noGreeting: {
    id: 'app.greeting.noGreeting',
    defaultMessage:
      'No greeting to be shown, maybe it is still loading or you forgot to turn on mockserver.',
  },
  submit: {
    id: 'app.home.submit',
    defaultMessage: 'Submit',
  },
  throw: {
    id: 'app.home.throw',
    defaultMessage: 'Throw an error!',
  },
  wat: {
    id: 'label.firstName',
    defaultMessage: 'Name',
  },
})

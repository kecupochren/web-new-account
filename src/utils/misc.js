// @flow

import get from 'lodash.get'

export const getResponseData = (res: Object): Object => get(res, 'data.responseObject')

export const asBase64 = (file: any, cb: Function) => {
  const reader = new FileReader()

  reader.readAsDataURL(file)
  reader.onloadend = () => cb(reader.result)
}

// @flow

const joinRules = rules => (value, data) =>
  rules
    // Test value against all rules
    .map(rule => rule(value, data))
    // Filter null errors & pick the first one
    .filter(error => Boolean(error))[0]

export function validator(rules: Object) {
  const validate = (data: Object = {}) => {
    const errors = {}

    Object.keys(rules).forEach(key => {
      // concat enables both functions and arrays of functions
      const rule = joinRules([].concat(rules[key]))
      const error = rule(data[key], data)

      if (error) {
        errors[key] = error
      }
    })

    return errors
  }

  return validate
}

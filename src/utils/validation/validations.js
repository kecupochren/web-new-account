// @flow

import * as React from 'react'
import { FormattedMessage } from 'react-intl'

type ValidationMessage = any

const isEmpty = value => value === undefined || value === null || value === ''
const checkCity = value => /[.,/#!$%?^&@§+*;:{}=\\_`"'~()<>|[\]]/i.test(value)
const checkEmail = value => !/^[A-Z0-9._%+-]{1,64}@[A-Z0-9-]{1,250}\.[A-Z]{2,8}$/i.test(value)
const checkZip = value => !/^[1-7]{1}[0-9]{2}\s?[0-9]{2}$/i.test(value)
const checkBirthNumber = value => /^[0-9]{6}\/?[0-9]{4}$/i.test(value)

export const required = (value: string): ValidationMessage => {
  if (!value) {
    return <FormattedMessage id="error.required" defaultMessage="Required field" />
  }
  return undefined
}

export function minLength(min: number): ValidationMessage {
  return (value?: string) => {
    if (value && value.length < min) {
      return (
        <FormattedMessage
          id="error.min_length"
          defaultMessage={`Must be at least ${min} characters`}
          values={{ min }}
        />
      )
    }
    return undefined
  }
}

export function maxLength(max: number): ValidationMessage {
  return (value?: string) => {
    if (value && value.length > max) {
      return (
        <FormattedMessage
          id="error.max_length"
          defaultMessage={`Must be at least ${max} characters`}
          values={{ max }}
        />
      )
    }
    return undefined
  }
}

export function email(value: string): ValidationMessage {
  if (isEmpty(value) || checkEmail(value)) {
    return <FormattedMessage id="error.invalid_email" defaultMessage="Invalid email address" />
  }
  return undefined
}

export function firstName(value: string): ValidationMessage {
  return required(value) || minLength(2)(value)
}

export function lastName(value: string): ValidationMessage {
  return required(value) || minLength(2)(value)
}

export function sms(value: string): ValidationMessage {
  console.log('here maybe?')
  return required(value) || minLength(4)(value) || maxLength(4)(value)
}

export function phone(value: string): ValidationMessage {
  const isValid = /^[0-9\-+]{9}$/i.test(value)

  if (!isValid) {
    return <FormattedMessage id="error.invalid_phone" defaultMessage="Invalid phone address" />
  }

  return undefined
}

export function stateOfBirth(value: string): ValidationMessage {
  return required(value)
}

export function city(value: string): ValidationMessage {
  const isValid = /[.,/#!$%?^&@§+*;:{}=\\_`"'~()<>|[\]]/i.test(value)

  if (isValid) {
    return <FormattedMessage id="error.invalid_birth_number" defaultMessage="Invalid city" />
  }

  return undefined
}

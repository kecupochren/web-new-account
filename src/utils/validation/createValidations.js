// @flow

/**
 * Transforms API supplied validation rules & err messages into validation functions
 */

import React from 'react'
import { FormattedMessage } from 'react-intl'

import type { APIValidations, ErrorMessage } from '../../types'

import * as defaultValidations from './validations'

export type Validations = {
  email: (value: string) => ErrorMessage,
  firstName: (value: string) => ErrorMessage,
  lastName: (value: string) => ErrorMessage,
  phone: (value: string) => ErrorMessage,
  sms: (value: string) => ErrorMessage,
  city: (value: string) => ErrorMessage,
}

const withFallbacks = (validations): Validations => ({ ...defaultValidations, ...validations })

/**
 * Map messages to errors, add local intl key fallbacks
 * (FIXME: The API should use a different format, such that we won't need to do do this mapping)
 */
const createErrors = errorMessages => ({
  ...errorMessages,
  required: errorMessages.required || 'error.required',
  email: errorMessages.noValidEmail || 'error.invalid_email',
  firstName: errorMessages.noValidFirstName || 'error.invalid_first_name',
  lastName: errorMessages.noValidLastName || 'error.invalid_last_name',
  phone: errorMessages.noValidPhoneNumber || 'error.invalid_phone',
})

export const createValidations = ({ rules, errorMessages }: APIValidations) => {
  const validations = {}
  const errors = createErrors(errorMessages)

  Object.keys(rules).forEach(rule => {
    const { regexp, min, max } = rules[rule]

    validations[rule] = function test(value = '') {
      if (!value) {
        return errors.required
      }

      if (regexp && !new RegExp(regexp).test(value)) {
        return errors[rule]
      }

      if (min && value.length < min) {
        return <FormattedMessage id="error.min_length" values={{ min }} />
      }

      if (max && value && value.length > max) {
        return <FormattedMessage id="error.max_length" values={{ max }} />
      }

      return null
    }
  })

  return withFallbacks(validations)
}

export default createValidations

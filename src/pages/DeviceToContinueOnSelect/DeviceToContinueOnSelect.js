// @flow

import React, { Fragment } from 'react'

import { Button, Text } from '../../components'
import { Wizard } from '../../containers'
import { WizardSteps } from '../../constants'
import type { WizardRenderProps } from '../../types'

export const DeviceToContinueOnSelect = () => (
  <Wizard step={WizardSteps.DEVICE_TO_CONTINUE_ON_SELECT}>
    {({ actions }: WizardRenderProps) => (
      <Fragment>
        <Text h2 messageId="DeviceToContinueOnSelect.title" />
        <Text messageId="DeviceToContinueOnSelect.subtitle" />

        <Button
          onClick={() => actions.handleSetContinueOnMobile(true)}
          primary
          wide
          messageId="DeviceToContinueOnSelect.btn.mobile"
        />

        <Button
          onClick={() => actions.handleSetContinueOnMobile(false)}
          secondary
          wide
          messageId="DeviceToContinueOnSelect.btn.desktop"
        />
      </Fragment>
    )}
  </Wizard>
)

export default DeviceToContinueOnSelect

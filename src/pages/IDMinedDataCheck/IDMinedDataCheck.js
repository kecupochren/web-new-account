// @flow

import React, { Fragment } from 'react'

import { Button, Text, Input } from '../../components'
import { Wizard } from '../../containers'
import { WizardSteps } from '../../constants'
import type { WizardRenderProps, Validations } from '../../types'

const validate = (validator, { firstName, lastName, city }: Validations) =>
  validator({ firstName, lastName, city })

export const IDMinedDataCheck = () => (
  <Wizard step={WizardSteps.ID_MINED_DATA_CHECK} validate={validate}>
    {({ submitting }: WizardRenderProps) => (
      <Fragment>
        <Text h2 messageId="IDMinedDataCheck.title" />
        <Text messageId="IDMinedDataCheck.subtitle" />

        <Input name="firstName" first />
        <Input name="lastName" />
        <Input name="birthDate" />
        <Input name="birthNumber" />
        <Input name="idCardNumber" />
        <Input name="idCardValidTo" />

        <Input name="street" />
        <Input name="city" />
        <Input name="zip" />
        <Input name="nationality" />
        <Input name="birthPlace" />

        <Button primary submit submitting={submitting} />
      </Fragment>
    )}
  </Wizard>
)

export default IDMinedDataCheck

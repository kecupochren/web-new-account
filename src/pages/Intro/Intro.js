// @flow

import React, { Fragment } from 'react'

import { Button, Input, Text } from '../../components'
import { Wizard } from '../../containers'
import { WizardSteps } from '../../constants'
import type { WizardRenderProps, Validations } from '../../types'

const prefill = {
  firstName: 'Kateřina',
  lastName: 'Růžičková',
  phone: '731123456',
  email: 'katerina.ruzickova@gmail.com',
}

const validate = (validator, { firstName, lastName, email, phone }: Validations) =>
  validator({ firstName, lastName, email, phone })

export const Intro = () => (
  <Wizard step={WizardSteps.INTRO} validate={validate} prefill={prefill}>
    {({ submitting }: WizardRenderProps) => (
      <Fragment>
        <Text h2 messageId="Intro.title" />
        <Text messageId="Intro.subtitle" />

        <Input name="firstName" first />
        <Input name="lastName" />
        <Input name="phone" />
        <Input name="email" />

        <Button submit primary submitting={submitting} />
      </Fragment>
    )}
  </Wizard>
)

export default Intro

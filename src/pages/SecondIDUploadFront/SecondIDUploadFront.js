// @flow

import React from 'react'

import { Uploads, WizardSteps } from '../../constants'
import { DocumentUpload, Wizard } from '../../containers'
import type { WizardRenderProps } from '../../types'

export const SecondIDUploadFront = () => (
  <Wizard step={WizardSteps.SECOND_ID_UPLOAD_FRONT}>
    {({ options, actions, formValues }: WizardRenderProps) => (
      <DocumentUpload
        document={options.secondIDDocument}
        side="front"
        uploadType={Uploads.SECOND_ID}
        options={options}
        actions={actions}
        formValues={formValues}
      />
    )}
  </Wizard>
)

export default SecondIDUploadFront

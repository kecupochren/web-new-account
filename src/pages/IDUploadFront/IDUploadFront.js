// @flow

import React from 'react'

import { Documents, Uploads, WizardSteps } from '../../constants'
import { DocumentUpload, Wizard } from '../../containers'
import type { WizardRenderProps } from '../../types'

export const IDUploadFront = () => (
  <Wizard wide step={WizardSteps.ID_UPLOAD_FRONT}>
    {({ options, actions, formValues }: WizardRenderProps) => (
      <DocumentUpload
        document={Documents.NATIONAL_ID}
        uploadType={Uploads.NATIONAL_ID}
        side="front"
        options={options}
        actions={actions}
        formValues={formValues}
      />
    )}
  </Wizard>
)

export default IDUploadFront

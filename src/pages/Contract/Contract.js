// @flow

import React, { Fragment, PureComponent } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'

import { Input, Text, Button, PDFDocument } from '../../components'
import { Wizard } from '../../containers'
import { Routes, WizardSteps } from '../../constants'
// import type { WizardRenderProps } from '../../types'

type Props = {
  push: (route: string) => void,
}

type State = {
  hasConfirmed: boolean,
  resentSms: boolean,
}

export class Contract extends PureComponent<Props, State> {
  state = {
    hasConfirmed: false,
    resentSms: false,
  }

  props: Props

  simulateResend = () => {
    this.setState({ resentSms: true }, () =>
      setTimeout(() => this.setState({ resentSms: false }), 3000)
    )
  }

  handleConfirm = () => {
    this.setState({ hasConfirmed: true })
  }

  handleReject = () => {
    this.props.push(Routes.ID_MINED_DATA_CHECK)
  }

  render() {
    const { resentSms, hasConfirmed } = this.state

    return (
      <Wizard step={WizardSteps.CONTRACT}>
        {() => (
          <Fragment>
            <Text h2 messageId="Contract.title" />

            <PDFDocument
              file={
                // $FlowFixMe
                require('../../assets/pdf/product-consent.pdf')
              }
            />

            {hasConfirmed && <Input name="sms" className="mt-2" />}

            {hasConfirmed ? (
              <Fragment>
                <Button primary submit messageId="Contract.sign" />

                {resentSms ? (
                  <Text center success messageId="success.smsSent" />
                ) : (
                  <Button
                    anchor
                    onClick={this.simulateResend}
                    center
                    className="mt-2"
                    messageId="button.resendSmsCode"
                  />
                )}
              </Fragment>
            ) : (
              <Fragment>
                <Button
                  anchor
                  wide
                  type="button"
                  center
                  messageId="Contract.reject"
                  onClick={this.handleReject}
                />
                <Button
                  primary
                  wide
                  type="button"
                  center
                  messageId="Contract.confirm"
                  onClick={this.handleConfirm}
                />
              </Fragment>
            )}
          </Fragment>
        )}
      </Wizard>
    )
  }
}

const withStore = connect(null, { push })

export default withStore(Contract)

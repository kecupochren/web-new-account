// @flow

import React, { Fragment } from 'react'

import { Documents, WizardSteps } from '../../constants'
import { Button, Text } from '../../components'
import { Wizard } from '../../containers'
import type { WizardRenderProps } from '../../types'

export const SecondIDSelect = () => (
  <Wizard step={WizardSteps.SECOND_ID_SELECT}>
    {({ submitting, actions }: WizardRenderProps) => {
      const handleSetSecondId = (document: string) => {
        actions.setSecondIdDocument(document)
        actions.handleSubmit()
      }

      return (
        <Fragment>
          <Text h2 messageId="SecondIDSelect.title" />
          <Text messageId="SecondIDSelect.subtitle" />

          <Button
            className="mt-2"
            messageId="documents.DRIVING_LICENCE"
            primary
            onClick={() => handleSetSecondId(Documents.DRIVING_LICENCE)}
            submitting={submitting}
          />
          <Button
            className="mt-2"
            messageId="documents.PASSPORT"
            primary
            onClick={() => handleSetSecondId(Documents.PASSPORT)}
            submitting={submitting}
          />
        </Fragment>
      )
    }}
  </Wizard>
)

export default SecondIDSelect

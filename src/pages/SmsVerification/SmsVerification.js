// @flow

import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'

import { Button, Text, Input } from '../../components'
import { Wizard } from '../../containers'
import { Modals, WizardSteps } from '../../constants'
import { appActions } from '../../redux/app'
import type { WizardRenderProps, OpenModalAction, Validations } from '../../types'

import ChangePhoneNumberModal from './ChangePhoneNumberModal/ChangePhoneNumberModal'

type Props = {
  openModal: OpenModalAction,
}

type State = {
  resentSms: boolean,
  resentChangedSms: boolean,
}

const prefill = { sms: '1234' }

const validate = (validator: Function, { sms }: Validations) => validator({ sms })
export class SmsVerification extends Component<Props, State> {
  state = {
    resentSms: false,
    resentChangedSms: false,
  }

  props: Props

  simulateResend = () => {
    this.setState({ resentSms: true }, () =>
      setTimeout(() => this.setState({ resentSms: false }), 3000)
    )
  }

  simulateChangePhone = () => {
    this.setState({ resentChangedSms: true }, () => {
      setTimeout(() => {
        this.setState({ resentChangedSms: false })
        setTimeout(() => this.props.openModal(null))
      }, 3000)
    })
  }

  handleChangePhone = () => this.props.openModal(Modals.CHANGE_PHONE_NUMBER)

  render() {
    console.log('render')
    const { resentSms, resentChangedSms } = this.state

    return (
      <Wizard step={WizardSteps.SMS_VERIFICATION} validate={validate} prefill={prefill}>
        {({ submitting, formValues: { firstName, lastName, phone, sms } }: WizardRenderProps) => (
          <Fragment>
            <Text
              h2
              noMargin
              messageId="SmsVerification.title"
              values={{ name: `${firstName} ${lastName}` }}
            />
            <Text messageId="SmsVerification.subtitle" values={{ phone }} />

            <Button
              anchor
              noMargin
              onClick={this.handleChangePhone}
              messageId="SmsVerification.button.changePhone"
            />

            <ChangePhoneNumberModal
              resentChangedSms={resentChangedSms}
              handleChangePhone={this.simulateChangePhone}
            />

            <Input name="sms" className="mt-2" />

            <Button submit primary submitting={submitting} />

            {resentSms ? (
              <Text center success messageId="success.smsSent" />
            ) : (
              <Button
                anchor
                onClick={this.simulateResend}
                center
                className="mt-2"
                messageId="button.resendSmsCode"
              />
            )}
          </Fragment>
        )}
      </Wizard>
    )
  }
}

const withStore = connect(null, { openModal: appActions.openModal })

export default withStore(SmsVerification)

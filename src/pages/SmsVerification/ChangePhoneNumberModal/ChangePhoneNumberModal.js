// @flow

import React from 'react'

import { Modals } from '../../../constants/'
import { Modal, Text, Button, Input } from '../../../components/'

type Props = {
  resentChangedSms: boolean,
  handleChangePhone: () => void,
}

export const ChangePhoneNumberModal = ({ handleChangePhone, resentChangedSms }: Props) => (
  <Modal id={Modals.CHANGE_PHONE_NUMBER} size="lg">
    <Text h2 noMargin messageId="ChangePhoneNumberModal.title" />

    <Input name="phone" maxWidth className="mt-2" />

    {resentChangedSms ? (
      <Text center success messageId="success.smsSent" />
    ) : (
      <Button anchor messageId="button.resendSmsCode" onClick={handleChangePhone} />
    )}
  </Modal>
)

export default ChangePhoneNumberModal

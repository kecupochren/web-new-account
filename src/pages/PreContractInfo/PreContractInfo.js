// @flow

import React, { Fragment } from 'react'

import { Text, Button } from '../../components'
import { Wizard } from '../../containers'
import { WizardSteps } from '../../constants'
import type { WizardRenderProps } from '../../types'

export const PreContractInfo = () => (
  <Wizard step={WizardSteps.PRE_CONTRACT_INFO}>
    {({ submitting }: WizardRenderProps) => (
      <Fragment>
        <Text h2 messageId="PreContractInfo.title" />

        <Text h3 messageId="PreContractInfo.list.title" />
        <Text checkmark messageId="PreContractInfo.list.moneta" />
        <Text checkmark messageId="PreContractInfo.list.communication" />
        <Text checkmark messageId="PreContractInfo.list.accountCreate" />
        <Text checkmark messageId="PreContractInfo.list.contract" />
        <Text checkmark messageId="PreContractInfo.list.insurance" />

        <Text h2 messageId="PreContractInfo.content.title" />
        <Text messageId="PreContractInfo.content.info" />

        <Text h3 messageId="PreContractInfo.content.bankInfo.title" />
        <Text messageId="PreContractInfo.content.bankInfo.content" />

        <Button submit primary submitting={submitting} />
      </Fragment>
    )}
  </Wizard>
)

export default PreContractInfo

// @flow

import React from 'react'

import { WizardSteps, Uploads, Documents } from '../../constants'
import { Wizard, DocumentUpload } from '../../containers'
import type { WizardRenderProps } from '../../types'

export const SelfieUpload = () => (
  <Wizard step={WizardSteps.SELFIE_UPLOAD}>
    {({ actions, options, formValues }: WizardRenderProps) => (
      <DocumentUpload
        document={Documents.SELFIE}
        uploadType={Uploads.SELFIE}
        options={options}
        actions={actions}
        formValues={formValues}
      />
    )}
  </Wizard>
)

export default SelfieUpload

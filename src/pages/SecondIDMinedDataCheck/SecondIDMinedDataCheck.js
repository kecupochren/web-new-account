// @flow

import React, { Fragment } from 'react'

import { Button, FormGroup, Input } from '../../components'
import { Wizard } from '../../containers'
import { WizardSteps } from '../../constants'
import type { WizardRenderProps } from '../../types'

export const SecondIDMinedDataCheck = () => (
  <Wizard step={WizardSteps.SECOND_ID_MINED_DATA_CHECK}>
    {({ submitting }: WizardRenderProps) => (
      <Fragment>
        <FormGroup
          titleId="SecondIDMinedDataCheck.title"
          subtitleId="SecondIDMinedDataCheck.subtitle"
          noMargin>
          <Input name="lastName" />
          <Input name="drivingLicenceNumber" />
          <Input name="drivingLicenceValidTo" />
        </FormGroup>

        <Button submit primary submitting={submitting} />
      </Fragment>
    )}
  </Wizard>
)

export default SecondIDMinedDataCheck

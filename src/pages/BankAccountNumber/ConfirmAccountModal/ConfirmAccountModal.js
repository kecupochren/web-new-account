// @flow

import React from 'react'

import { Modals } from '../../../constants/'
import { Modal, Text, Button } from '../../../components/'

type Props = {
  handleConfirm: () => void,
}

export const ConfirmAccountModal = ({ handleConfirm }: Props) => (
  <Modal id={Modals.CONFIRM_ACCOUNT_NUMBER} size="lg">
    <Text h3 noMargin messageId="ConfirmAccountModal.title" />
    <Button primary messageId="button.continue" onClick={handleConfirm} />
  </Modal>
)

export default ConfirmAccountModal

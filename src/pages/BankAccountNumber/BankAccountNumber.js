// @flow

import React, { Fragment } from 'react'
import { connect } from 'react-redux'

import { Input, Text, Button } from '../../components'
import { Wizard } from '../../containers'
import { Modals, WizardSteps } from '../../constants'
import { appActions } from '../../redux/app'
import type { WizardRenderProps, OpenModalAction } from '../../types'

import ConfirmAccountModal from './ConfirmAccountModal/ConfirmAccountModal'

type Props = {
  openModal: OpenModalAction,
}

export const BankAccountNumber = ({ openModal }: Props) => (
  <Wizard step={WizardSteps.BANK_ACCOUNT_NUMBER}>
    {({ submitting, actions }: WizardRenderProps) => (
      <Fragment>
        <Text h2 messageId="BankAccountNumber.title" />
        <Text messageId="BankAccountNumber.subtitle" />

        <Text h2 messageId="BankAccountNumber.form.title" />
        <Text messageId="BankAccountNumber.form.list.title" />
        <Text className="mt-2" checkmark messageId="BankAccountNumber.form.list.yourName" />
        <Text checkmark messageId="BankAccountNumber.form.list.czechBank" />
        <Text checkmark messageId="BankAccountNumber.form.list.verified" />

        <Input
          first
          name="accountNumber"
          messages={{ label: { id: 'BankAccountNumber.form.input' } }}
        />

        <Text messageId="BankAccountNumber.form.input.note" />

        <Button
          center
          messageId="BankAccountNumber.button.showPaymentInfo"
          primary
          type="button"
          onClick={() => openModal(Modals.CONFIRM_ACCOUNT_NUMBER)}
          submitting={submitting}
        />

        <ConfirmAccountModal handleConfirm={actions.handleSubmit} />
      </Fragment>
    )}
  </Wizard>
)

const withStore = connect(null, {
  openModal: appActions.openModal,
})

export default withStore(BankAccountNumber)

// @flow

import React, { Fragment } from 'react'

import { Text } from '../../components'
import { Wizard } from '../../containers'
import { WizardSteps } from '../../constants'
// import type { WizardRenderProps } from '../../types'

export const ContinueOnDesktop = () => (
  <Wizard step={WizardSteps.CONTINUE_ON_DESKTOP}>
    {() => (
      <Fragment>
        <Text h2 messageId="ContinueOnDesktop.title" />
        <Text messageId="ContinueOnDesktop.subtitle" />
      </Fragment>
    )}
  </Wizard>
)

export default ContinueOnDesktop

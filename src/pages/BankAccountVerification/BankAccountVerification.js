// @flow

import React, { Fragment } from 'react'

import { Button, Text } from '../../components'
import { Wizard } from '../../containers'
import { WizardSteps } from '../../constants'
import type { WizardRenderProps } from '../../types'

import styles from './styles.scss'

export const BankAccountVerification = () => (
  <Wizard step={WizardSteps.BANK_ACCOUNT_VERIFICATION}>
    {({ submitting }: WizardRenderProps) => (
      <Fragment>
        <Text h2 messageId="BankAccountVerification.title" />
        <Text messageId="BankAccountVerification.subtitle" />

        <div className={styles.paymentInfo}>
          <img alt="" src={require('../../assets/img/qr-code.png')} className={styles.qrCode} />

          <div className={styles.info}>
            <div className={styles.infoEntry}>
              <Text
                inline
                className={styles.infoEntryLabel}
                messageId="BankAccountVerification.fromAccount"
              />
              <span>4562344587/0800</span>
            </div>

            <div className={styles.infoEntry}>
              <Text
                inline
                className={styles.infoEntryLabel}
                messageId="BankAccountVerification.toAccount"
              />
              <span>5312344512/0600</span>
            </div>

            <div className={styles.infoEntry}>
              <Text
                inline
                className={styles.infoEntryLabel}
                messageId="BankAccountVerification.amount"
              />
              <span>3 - 100 Kč</span>
            </div>

            <div className={styles.infoEntry}>
              <Text
                inline
                className={styles.infoEntryLabel}
                messageId="BankAccountVerification.dueDate"
              />
              <span>13. 4. 2018</span>
            </div>
          </div>
        </div>

        <Button submit primary submitting={submitting} />
      </Fragment>
    )}
  </Wizard>
)

export default BankAccountVerification

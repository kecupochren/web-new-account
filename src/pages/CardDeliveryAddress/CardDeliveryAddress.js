// @flow

import React, { Fragment } from 'react'

import { Button, Text, FormGroup, Input } from '../../components'
import { Wizard } from '../../containers'
import { WizardSteps } from '../../constants'
import type { WizardRenderProps } from '../../types'

const PostalAddressForm = () => (
  <FormGroup>
    <Input name="street" />
    <Input name="streetNumber" />
    <Input name="city" />
    <Input name="zip" />
  </FormGroup>
)

const Address = () => (
  <div style={{ margin: '52px 0 26px' }}>
    <Text h3>Kateřina Růžičková</Text>
    <Text noMargin>Pražská 257/54</Text>
    <Text noMargin>Ústí nad Labem</Text>
    <Text noMargin>400 01</Text>
  </div>
)

// const validate = (validator, { sms }, { samePostalAddress }) =>
//   validator(
//     !samePostalAddress && {
//       sms: [sms],
//     }
//   )

export const CardDeliveryAddress = () => (
  <Wizard step={WizardSteps.CARD_DELIVERY_ADDRESS}>
    {({ actions, submitting, options: { samePostalAddress } }: WizardRenderProps) => (
      <Fragment>
        <Text h2 messageId="CardDeliveryAddress.title" />
        <Text messageId="CardDeliveryAddress.subtitle" />

        {samePostalAddress ? <Address /> : <PostalAddressForm />}

        <Button
          anchor
          messageId={`CardDeliveryAddress.changeAddress.${samePostalAddress ? 'same' : 'not'}`}
          onClick={() => actions.setSamePostalAddress(!samePostalAddress)}
        />

        <Button submit primary submitting={submitting} />
      </Fragment>
    )}
  </Wizard>
)

export default CardDeliveryAddress

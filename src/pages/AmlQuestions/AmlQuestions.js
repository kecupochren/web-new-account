// @flow

import React, { Fragment } from 'react'

import { Button, Text, RadioSelect } from '../../components'
import { Wizard } from '../../containers'
import { WizardSteps } from '../../constants'
import type { WizardRenderProps } from '../../types'

const options = {
  taxResidency: [
    { value: 'inCR', label: 'AmlQuestions.taxResidency.inCR' },
    { value: 'outsideCR', label: 'AmlQuestions.taxResidency.outsideCR' },
  ],
  transactionsType: [
    { value: 'cash', label: 'AmlQuestions.transactionsType.cash' },
    { value: 'domestic', label: 'AmlQuestions.transactionsType.domestic' },
    { value: 'international', label: 'AmlQuestions.transactionsType.international' },
  ],
  incomeSource: [
    { value: 'salary', label: 'AmlQuestions.incomeSource.salary' },
    { value: 'business', label: 'AmlQuestions.incomeSource.business' },
    { value: 'pension', label: 'AmlQuestions.incomeSource.pension' },
    { value: 'socialFunds', label: 'AmlQuestions.incomeSource.socialFunds' },
    { value: 'gift', label: 'AmlQuestions.incomeSource.gift' },
  ],
}

export const AmlQuestions = () => (
  <Wizard step={WizardSteps.AML_QUESTIONS}>
    {({ submitting }: WizardRenderProps) => (
      <Fragment>
        <Text h2 messageId="AmlQuestions.title" />
        <Text messageId="AmlQuestions.subtitle" />

        <RadioSelect
          messageId="AmlQuestions.taxResidency.title"
          name="taxResidency"
          options={options.taxResidency}
        />

        <RadioSelect
          messageId="AmlQuestions.transactionsType.title"
          name="transactionsType"
          options={options.transactionsType}
        />

        <RadioSelect
          messageId="AmlQuestions.incomeSource.title"
          name="incomeSource"
          options={options.incomeSource}
        />

        <Button submit primary submitting={submitting} />
      </Fragment>
    )}
  </Wizard>
)

export default AmlQuestions

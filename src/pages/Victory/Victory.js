// @flow

import React, { Fragment } from 'react'

import { Button, Input, Text } from '../../components'
import { Wizard } from '../../containers'
import { WizardSteps } from '../../constants'
// import type { WizardRenderProps } from '../../types'

import styles from './styles.scss'

export const Victory = () => {
  const handleOpenInternetBanking = () => {
    window.open('https://ibs.internetbanka.cz/ibs/ControllerServlet', '_newtab')
  }

  return (
    <Wizard step={WizardSteps.VICTORY} withoutStepper>
      {() => (
        <Fragment>
          <Text h2 messageId="Victory.title" />
          <Text messageId="Victory.info" />

          {/* Web IB */}
          <Text h4 messageId="Victory.internetBanking.title" />
          <Button
            style={{ minWidth: '270px' }}
            onClick={handleOpenInternetBanking}
            type="button"
            primary
            messageId="Victory.internetBanking.button"
          />

          {/* Mobile IB */}
          <Text h3 className="mt-4" messageId="Victory.mobileBanking.title" />
          <a
            target="_newtab"
            href="https://itunes.apple.com/us/app/smart-banka-mobilni-bankovnictvi/id1122443334?mt=8">
            <img
              className={styles.mobileAppIcon}
              alt="MONETA na App Store"
              src={require('../../assets/img/button-appstore.png')}
            />
          </a>
          <a
            target="_newtab"
            href="https://play.google.com/store/apps/details?id=cz.moneta.smartbanka&utm_campaign=null">
            <img
              className={styles.mobileAppIcon}
              alt="MONETA na Google Play"
              src={require('../../assets/img/button-googleplay.png')}
            />
          </a>

          <Text h4 className="mt-2" messageId="Victory.mobileBanking.sms" />
          <Input name="phone" />
          <Button noMargin type="button" messageId="button.send" />
        </Fragment>
      )}
    </Wizard>
  )
}

export default Victory

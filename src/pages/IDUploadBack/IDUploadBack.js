// @flow

import React from 'react'

import { Documents, Uploads, WizardSteps } from '../../constants'
import { DocumentUpload, Wizard } from '../../containers'
import type { WizardRenderProps } from '../../types'

export const IDUploadBack = () => (
  <Wizard wide step={WizardSteps.ID_UPLOAD_BACK} spinnerMessageId="IDMinedDataCheck.spinner">
    {({ options, actions, formValues }: WizardRenderProps) => (
      <DocumentUpload
        document={Documents.NATIONAL_ID}
        uploadType={Uploads.NATIONAL_ID}
        side="back"
        options={options}
        actions={actions}
        formValues={formValues}
      />
    )}
  </Wizard>
)

export default IDUploadBack

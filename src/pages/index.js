// @flow

export { default as AmlQuestions } from './AmlQuestions/AmlQuestions'
export { default as BankAccountNumber } from './BankAccountNumber/BankAccountNumber'
export { default as BankAccountVerification, } from './BankAccountVerification/BankAccountVerification' // prettier-ignore
export { default as CardDeliveryAddress } from './CardDeliveryAddress/CardDeliveryAddress'
export { default as ContinueOnDesktop } from './ContinueOnDesktop/ContinueOnDesktop'
export { default as Contract } from './Contract/Contract'
export { default as DeviceToContinueOnSelect } from './DeviceToContinueOnSelect/DeviceToContinueOnSelect' // prettier-ignore
export { default as IDMinedDataCheck } from './IDMinedDataCheck/IDMinedDataCheck'
export { default as IDUploadBack } from './IDUploadBack/IDUploadBack'
export { default as IDUploadFront } from './IDUploadFront/IDUploadFront'
export { default as Intro } from './Intro/Intro'
export { default as PreContractInfo } from './PreContractInfo/PreContractInfo'
export { default as SecondIDMinedDataCheck } from './SecondIDMinedDataCheck/SecondIDMinedDataCheck' // prettier-ignore
export { default as SecondIDSelect } from './SecondIDSelect/SecondIDSelect'
export { default as SecondIDUploadFront } from './SecondIDUploadFront/SecondIDUploadFront'
export { default as SelfieUpload } from './SelfieUpload/SelfieUpload'
export { default as SmsVerification } from './SmsVerification/SmsVerification'
export { default as Victory } from './Victory/Victory'

// @flow

// https://reactjs.org/docs/portals.html

import { Component } from 'react'
import ReactDOM from 'react-dom'

// $FlowFixMe
const portalRoot: HTMLElement = document.getElementById('portal-root')

type Props = {
  children: any,
}

class Portal extends Component<Props> {
  constructor() {
    super()
    this.element = document.createElement('div')
  }

  componentDidMount() {
    portalRoot.appendChild(this.element)
  }

  componentWillUnmount() {
    portalRoot.removeChild(this.element)
  }

  element: HTMLElement
  props: Props

  render() {
    const { children } = this.props

    return ReactDOM.createPortal(children, this.element)
  }
}

export default Portal

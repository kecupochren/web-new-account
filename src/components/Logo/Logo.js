// @flow

import React from 'react'

import styles from './styles.scss'

export const Logo = () => (
  <div className={styles.wrapper}>
    <img alt="MONETA logo" src={require('../../assets/img/moneta-logo-negativ.png')} />
  </div>
)

export default Logo

// @flow

import React from 'react'
import cx from 'classnames'
import { connect } from 'react-redux'

import { Text } from '../../components/'
import { WizardSections } from '../../constants/'
import { getActiveWizardSection } from '../../redux/selectors'

import styles from './styles.scss'

type Props = {
  activeSection: string,
}

export const Stepper = ({ activeSection }: Props) => (
  <div className={styles.wrapper}>
    {Object.keys(WizardSections).map(section => (
      <div
        key={`section-${section}`}
        className={cx(styles.section, {
          [styles.isActive]: activeSection === section,
        })}>
        <Text noMargin messageId={`wizardSections.${section}`} />
      </div>
    ))}
  </div>
)

const withStore = connect(state => ({
  activeSection: getActiveWizardSection(state),
}))

export default withStore(Stepper)

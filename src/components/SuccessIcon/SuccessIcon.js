// @flow

import React from 'react'
import cx from 'classnames'

import style from './styles.scss'

type Props = {
  className?: string,
}

export const SuccessIcon = ({ className }: Props) => (
  <svg className={cx(style.wrapper, className)} viewBox="0 0 52 52">
    <circle className={style.circle} cx="26" cy="26" r="25" fill="none" />
    <path className={style.check} fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
  </svg>
)

SuccessIcon.displayName = 'SuccessIcon'

export default SuccessIcon

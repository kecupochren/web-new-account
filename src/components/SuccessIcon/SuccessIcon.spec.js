import React from 'react'
import { shallow } from 'enzyme'
import SuccessIcon from './SuccessIcon'

describe('<SuccessIcon />', () => {
  const props = {}

  it('renders correctly', () => {
    const tree = shallow(<SuccessIcon {...props} />)

    expect(tree).toMatchSnapshot()
  })
})

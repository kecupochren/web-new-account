// @flow

import React from 'react'
import RCToggle from 'react-toggle'

import { Text } from '../../components/'

import styles from './styles.scss'

type Props = {
  messageId: string,
  checked: boolean,
  onChange: Function,
}

export const Toggle = ({ checked, messageId, onChange }: Props) => {
  const handleChange = event => onChange(event.target.checked)
  const id = `toggleId-${messageId}`

  return (
    <label className={styles.label} htmlFor={id}>
      <RCToggle id={id} checked={checked} onChange={handleChange} />
      <Text className={styles.text} noMargin messageId={messageId} />
    </label>
  )
}

export default Toggle

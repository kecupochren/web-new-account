// @flow

import React from 'react'
import cx from 'classnames'

import { Text } from '../../components/'

import styles from './styles.scss'

type Props = {
  children: any,
  titleId?: string,
  subtitleId?: string,
  noMargin?: boolean,
}

export const FormGroup = ({ children, titleId, subtitleId, noMargin }: Props) => (
  <div
    className={cx(styles.wrapper, {
      'mt-2': !noMargin,
    })}>
    {titleId && <Text h2 noMargin messageId={titleId} />}
    {subtitleId && <Text messageId={subtitleId} />}

    <div className="mt-2">{children}</div>
  </div>
)

export default FormGroup

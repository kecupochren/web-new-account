// @flow

import React from 'react'
import { connect } from 'react-redux'

import { Button } from '../../components'
import { setEnvironment } from '../../redux/config'
import { Environments } from '../../constants'

type Props = {
  setEnvironment: (env: string) => mixed,
}

// istanbul ignore next
const EnvSelect = ({ setEnvironment }: Props) => (
  <div>
    {Object.keys(Environments).map(env => (
      <Button anchor noMargin key={env} onClick={() => setEnvironment(env)}>
        {env}
      </Button>
    ))}
  </div>
)

const withStore = connect(null, { setEnvironment })

export default withStore(EnvSelect)

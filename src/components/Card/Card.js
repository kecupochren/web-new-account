// @flow

import React from 'react'
import cx from 'classnames'

import styles from './styles.scss'

type Props = {
  className?: string,
  compact?: boolean,
  wide?: boolean,
  noAnimation?: boolean,
  children: any,
  style?: Object,
}

export const Card = ({ compact, className = '', children, style, noAnimation, wide }: Props) => (
  <div
    className={cx(styles.wrapper, {
      [className]: className,
      [styles.compact]: compact,
      [styles.wide]: wide,
    })}
    style={style}>
    <div
      className={cx(styles.content, {
        [styles.withAnimation]: !noAnimation,
      })}>
      {children}
    </div>
  </div>
)

export default Card

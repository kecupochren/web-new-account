// @flow

import React, { PureComponent } from 'react'
import get from 'lodash.get'
import cx from 'classnames'
import RCWebcam from 'react-webcam'

import { Documents } from '../../constants/'
import { Button } from '../../components/'
import type { UploadedDocument } from '../../types'

import styles from './styles.scss'

type Props = {
  className?: string,
  document: string,
  isMobile?: boolean,
  uploadedDocument: UploadedDocument,
  onConfirm: () => void,
  onRetry: () => void,
  onCapture: (base64: string) => void,
}

type State = {
  activeIndex: number,
  videoInputs: Array<Object>,
  ready: boolean,
}

export class Webcam extends PureComponent<Props, State> {
  constructor() {
    super()

    this.webcam = null
    this.state = {
      videoInputs: [],
      activeIndex: 0,
      ready: false,
    }
  }

  componentDidMount() {
    // $FlowFixMe
    navigator.mediaDevices.enumerateDevices().then(this.gotDevices)
  }

  setRef = (webcam: any) => {
    this.webcam = webcam
  }

  videoInputs = []

  gotDevices = (deviceInfos: Array<any>) => {
    const videoInputs = deviceInfos
      .filter(device => device.kind === 'videoinput')
      .map(device => ({ id: device.deviceId, name: device.label }))

    // Naively find front/back camera
    const side = this.props.document === Documents.SELFIE ? 'front' : 'back'
    const backIndex = videoInputs.indexOf(videoInputs.find(x => x.name.includes(side)))
    const index = backIndex > -1 ? backIndex : 0

    this.setState({
      videoInputs,
      activeIndex: index,
      ready: true,
    })
  }

  handleChangeCamera = () => {
    const { activeIndex, videoInputs } = this.state
    const hasMore = videoInputs.length > 1 && activeIndex <= videoInputs.length

    if (hasMore) {
      this.setState({ activeIndex: activeIndex + 1 })
    }
  }

  handleCapture = () => this.props.onCapture(this.webcam.getScreenshot())

  handleRetry = () => this.props.onRetry()

  handleUse = () => this.props.onConfirm()

  props: Props
  webcam: any

  render() {
    const { videoInputs, ready, activeIndex } = this.state
    const { className = '', uploadedDocument, isMobile } = this.props

    const preview = uploadedDocument && uploadedDocument.preview
    const videoSource = get(videoInputs, `${activeIndex}.id`)

    return (
      ready && (
        <div
          className={cx(styles.wrapper, {
            [className]: className,
            'mt-4': !isMobile,
            [styles.isMobile]: isMobile,
          })}>
          <div className={styles.webcam}>
            <RCWebcam
              ref={this.setRef}
              audio={false}
              width={480}
              height={360}
              videoSource={videoSource}
              audioSource="default"
              style={{ opacity: preview ? '0' : '1' }}
            />

            {preview && (
              <div className={styles.preview}>
                <img src={preview} alt="preview" />
              </div>
            )}
          </div>

          <div className={styles.actions}>
            {videoInputs.length > 1 && (
              <Button anchor center large onClick={this.handleChangeCamera}>
                Změnit kameru
              </Button>
            )}

            {preview ? (
              <div>
                <Button
                  secondary
                  messageId="button.retry"
                  className={styles.btnRetry}
                  onClick={this.handleRetry}
                />
                <Button messageId="button.use" onClick={this.handleUse} />
              </div>
            ) : (
              <Button messageId="button.capture" onClick={this.handleCapture} />
            )}
          </div>
        </div>
      )
    )
  }
}

export default Webcam

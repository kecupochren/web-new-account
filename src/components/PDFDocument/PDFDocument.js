// @flow

import React, { PureComponent } from 'react'
import { Document, Page } from 'react-pdf'

type Props = {
  file: any,
}

type State = {
  numPages: ?number,
  pageNumber: number,
}

export class PDFDocument extends PureComponent<Props, State> {
  state = {
    numPages: null,
    pageNumber: 1,
  }

  onDocumentLoad = ({ numPages }: Object) => {
    this.setState({ numPages })
  }

  render() {
    const { pageNumber, numPages } = this.state
    const { file } = this.props

    return (
      <Document file={file} onLoadSuccess={this.onDocumentLoad}>
        <Page pageNumber={pageNumber} />
        <p>
          Page {pageNumber} of {numPages}
        </p>
      </Document>
    )
  }
}

export default PDFDocument

// @flow

import * as React from 'react'
import { injectIntl } from 'react-intl'
import { connect } from 'react-redux'
import { BModal, BModalHeader, BModalBody } from '@prague-digi/ui-components'

import { appActions, type AppActions } from '../../redux/app'
import type { State } from '../../types'

import styles from './styles.scss'

type Props = {
  children: React.Node,
  id: string,
  openModalId: ?string,
  intl: Object,
} & AppActions

export class Modal extends React.PureComponent<Props> {
  handleClose = () => this.props.openModal(null)

  props: Props

  render() {
    const { children, id, openModalId, ...rest } = this.props

    return (
      <BModal
        backdrop
        isOpen={id === openModalId}
        toggle={this.handleClose}
        dialogClassName={`${styles.dialog} text--center`}
        {...rest}>
        <BModalHeader className={styles.header} toggle={this.handleClose} />
        <BModalBody>{children}</BModalBody>
      </BModal>
    )
  }
}

const withStore = connect(
  ({ app }: State) => ({
    openModalId: app.openModalId,
  }),
  appActions
)

export default withStore(injectIntl(Modal))

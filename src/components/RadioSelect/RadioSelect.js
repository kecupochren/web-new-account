// @flow

import React from 'react'

import { Text, Radio } from '../../components'

import styles from './styles.scss'

type Option = {
  label: string,
  value: string,
}

type Props = {
  options: Option[],
  name: string,
  messageId: string,
}

export const RadioSelect = ({ name, messageId, options }: Props) => (
  <div className={styles.wrapper}>
    <Text h3 messageId={messageId} />

    <div className={styles.options}>
      {options.map(({ label, value }) => (
        <Radio key={`radio-${name}-${value}`} name={name} label={label} value={value} />
      ))}
    </div>
  </div>
)

export default RadioSelect

// @flow

import * as React from 'react'
import cx from 'classnames'
import { PButton } from '@prague-digi/ui-components'
import type { MessageDescriptor } from 'react-intl'

import styles from './styles.scss'

type Props = {
  anchor?: boolean,
  center?: boolean,
  className?: string,
  disabled?: boolean,
  large?: boolean,
  message?: MessageDescriptor,
  messageId?: string,
  narrow?: boolean,
  noMargin?: boolean,
  onClick?: Function,
  secondary?: boolean,
  submit?: boolean,
  submitting?: boolean,
  type?: string,
  white?: boolean,
  wide?: boolean,
}

export const Button = ({
  anchor,
  center,
  className,
  disabled,
  large,
  message,
  messageId,
  narrow,
  noMargin,
  onClick,
  secondary,
  submit,
  submitting,
  type,
  wide,
  white,
  ...rest
}: Props) => {
  const handleClick = (e: any) => {
    // istanbul ignore else
    if (disabled || submitting) {
      e.preventDefault()
    } else if (onClick) {
      onClick(e)
    }
  }

  let messageIdFinal = messageId

  if (submit) {
    messageIdFinal = 'button.continue'
  }

  return (
    <div
      className={cx(styles.wrapper, {
        'mt-2': !noMargin,
        [styles.disabled]: disabled,
        [styles.submitting]: submitting,
        [styles.center]: center,
        [styles.submit]: submit,
        [className || '']: className,
      })}>
      <PButton
        {...rest}
        message={messageIdFinal ? { id: messageIdFinal } : message}
        testid="button"
        className={cx({
          [styles.button]: !anchor,
          [styles.anchor]: anchor,
          [styles.large]: large,
          [styles.secondary]: secondary,
          [styles.narrow]: narrow,
          [styles.wide]: wide,
          'text--white': white,
        })}
        onClick={handleClick}
        type={type || (onClick ? 'button' : 'submit')}
      />
    </div>
  )
}

export default Button

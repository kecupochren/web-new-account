// @flow

import React from 'react'

import { Text } from '../../components'

import styles from './styles.scss'

type Props = {
  value: string,
  name: string,
  label: string,
}

export const Radio = ({ name, value, label }: Props) => (
  <label htmlFor={`${name}-${value}`} className={styles.label}>
    <input type="radio" id={`${name}-${value}`} name={name} value={value} />
    <div className={styles.checkmark} />
    <Text noMargin inline messageId={label} />
  </label>
)

export default Radio

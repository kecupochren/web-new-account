// @flow

import React from 'react'
import cx from 'classnames'

import { Text } from '../../components/'
import { asBase64 } from '../../utils/'
import type { UploadedDocument } from '../../types/'

import styles from './styles.scss'

type Props = {
  onUpload: (file: any) => void,
  uploadedDocument: UploadedDocument,
}

export const FileInputButton = ({ onUpload, uploadedDocument }: Props) => {
  const handleChange = event => {
    const file = event.target.files && event.target.files[0]

    asBase64(file, (data: any) => onUpload(data))
  }

  const preview = uploadedDocument && uploadedDocument.preview

  return (
    <div>
      <div className={styles.preview}>{preview && <img alt="" src={preview} />}</div>

      {!preview && (
        <label className={cx(styles.wrapper)} htmlFor="file-input">
          <Text noMargin inline messageId="button.capture" />
          <input type="file" id="file-input" capture="camera" onChange={handleChange} />
        </label>
      )}
    </div>
  )
}

export default FileInputButton

// @flow

import React from 'react'

import { Text } from '../../../components/'
import { DevMenu } from '../../../containers/'

import styles from './styles.scss'

// type Props = {
//   foo: ?String,
// }

export const Footer = () => (
  <div className={styles.wrapper}>
    <Text h3 center messageId="footer.title" />

    <div className={styles.contacts}>
      <Text messageId="footer.phone" values={{ phone: '224 444 333' }} />
      <Text messageId="footer.branch" values={{ link: 'najděte nejbližší' }} />
    </div>

    <DevMenu />
  </div>
)

export default Footer

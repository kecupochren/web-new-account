import React from 'react'
import { shallow } from 'enzyme'
import { Page } from './Page'

describe('<Page />', () => {
  const props = {
    children: <div />,
  }

  it('renders correctly', () => {
    const tree = shallow(<Page {...props} />)

    expect(tree).toMatchSnapshot()
  })
})

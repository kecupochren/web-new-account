// @flow
/* eslint-disable import/no-dynamic-require */

import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { addLocaleData } from 'react-intl'
import store from 'store'

import { setLanguage } from '../../../redux/config'

import styles from './styles.scss'

// $FlowFixMe
addLocaleData(require('react-intl/locale-data/cs'))
// $FlowFixMe
addLocaleData(require('react-intl/locale-data/en'))

type State = {
  lang: string,
}

type Props = {
  setLanguage: (lang: string) => void,
}

export class LanguageSelector extends PureComponent<Props, State> {
  state = { lang: 'cs' }

  componentWillMount() {
    const lang = store.get('lang')

    if (lang) {
      this.setLanguage(lang)
    }
  }

  setLanguage = (lang: string) => {
    this.setState({ lang })
    this.props.setLanguage(lang)
    store.set('lang', lang)
  }

  props: Props

  render() {
    const { lang } = this.state
    const targetLang = lang === 'cs' ? 'en' : 'cs'

    return (
      <div className={styles.wrapper}>
        <button onClick={() => this.setLanguage(targetLang)}>
          {
            // $FlowFixMe
            <img alt="" src={require(`../../../assets/img/flag-${targetLang}.svg`)} />
          }
          {targetLang}
        </button>
      </div>
    )
  }
}

const withStore = connect(null, { setLanguage })

export default withStore(LanguageSelector)

// @flow

import React from 'react'
import cx from 'classnames'

import { Text, Logo } from '../../components'

import styles from './styles.scss'
// import LanguageSelector from './LanguageSelector'
import Footer from './Footer'

type Props = {
  className: string,
  compact?: boolean,
  children: any,
}

export const Page = ({ children, className, compact }: Props) => (
  <div className={styles.wrapper}>
    <header>
      <div className={styles.headerContent}>
        <Logo />
        {/* <LanguageSelector /> */}
      </div>
    </header>

    <main>
      <div
        className={cx(styles.content, {
          [className]: className,
          [styles.compact]: compact,
        })}>
        <div>
          <Text className={styles.title} h1 message={{ id: 'app.title' }} />

          <div>{children}</div>
        </div>
      </div>
    </main>

    <Footer />
  </div>
)

Page.defaultProps = {
  className: '',
}

export default Page

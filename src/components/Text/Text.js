// @flow

import React from 'react'
import cx from 'classnames'
import { FormattedHTMLMessage } from 'react-intl'
import type { MessageDescriptor } from 'react-intl'

import styles from './styles.scss'

type Props = {
  h1?: boolean,
  h2?: boolean,
  h3?: boolean,
  h4?: boolean,
  bold?: boolean,
  center?: boolean,
  checkmark?: boolean,
  children?: any,
  className: string,
  message?: MessageDescriptor | string,
  messageId?: string,
  noMargin?: boolean,
  small?: boolean,
  white?: boolean,
  success?: boolean,
  breakWord?: boolean,
  breakLine?: boolean,
  values?: Object,
  inline?: boolean,
}

export const Text = ({
  h1,
  h2,
  h3,
  h4,
  message,
  messageId,
  children,
  className,
  checkmark,
  values,
  bold,
  center,
  noMargin,
  white,
  success,
  small,
  breakWord,
  breakLine,
  inline,
}: Props) => {
  let Component = 'div'

  if (h1) {
    Component = 'h1'
  }

  if (h2) {
    Component = 'h2'
  }

  if (h3) {
    Component = 'h3'
  }

  if (h4) {
    Component = 'h4'
  }

  if (inline) {
    Component = 'span'
  }

  return (
    <Component
      className={cx(styles.wrapper, {
        [className]: className,
        [styles.paragraph]: Component === 'div',
        [styles.withCheckmark]: checkmark,
        'text--white': white,
        'text--sm': small,
        'text--center': center,
        'text--bold': bold,
        'mt-0': noMargin,
        'text--break': breakWord,
        'text--linebreak': breakLine,
        'text--success': success,
      })}>
      {checkmark && (
        <img className={styles.checkmark} alt="" src={require('../../assets/img/checkmark.svg')} />
      )}

      {children && children}

      {messageId && <FormattedHTMLMessage id={messageId} values={values} />}

      {message &&
        (typeof message === 'string' ? (
          message
        ) : (
          <FormattedHTMLMessage {...message} values={values} />
        ))}
    </Component>
  )
}

Text.defaultProps = {
  className: '',
}

export default Text

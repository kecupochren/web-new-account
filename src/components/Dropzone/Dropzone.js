// @flow

import React, { PureComponent } from 'react'
import cx from 'classnames'
import RCDropzone from 'react-dropzone'

import { Button, Text } from '../../components'
import { asBase64 } from '../../utils'
import type { UploadedDocument } from '../../types'

import styles from './styles.scss'

type Props = {
  onDrop: (file: Object) => void,
  onRetry: () => void,
  onConfirm: () => void,
  messageValues?: Object,
  uploadedDocument: UploadedDocument,
}

export default class Dropzone extends PureComponent<Props> {
  props: Props

  handleDrop = (acceptedFiles: Array<Object>) => {
    const file = acceptedFiles[0]

    if (file) {
      asBase64(file, (data: any) => this.props.onDrop(data))
    }
  }

  handleRetry = () => this.props.onRetry()

  handleConfirm = () => this.props.onConfirm()

  render() {
    const { messageValues, uploadedDocument } = this.props

    const preview = uploadedDocument && uploadedDocument.preview

    return (
      <div className={styles.wrapper}>
        <div
          className={cx(styles.dropzone, {
            [styles.withPreview]: preview,
          })}>
          <RCDropzone onDrop={this.handleDrop}>
            {preview ? (
              <img alt="" src={preview} />
            ) : (
              <Text noMargin messageId="DocumentUpload.dropzone" values={messageValues} />
            )}
          </RCDropzone>
        </div>

        {preview && (
          <div className={styles.actions}>
            <Button secondary messageId="button.retry" onClick={this.handleRetry} />
            <Button messageId="button.use" onClick={this.handleConfirm} />
          </div>
        )}
      </div>
    )
  }
}

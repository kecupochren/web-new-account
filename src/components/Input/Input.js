// @flow

import React, { Component } from 'react'
import cx from 'classnames'
import get from 'lodash.get'
import { injectIntl } from 'react-intl'
import { Field } from 'redux-form'
import RTInput from 'react-toolbox/lib/input/index'
import type { MessageDescriptor, IntlShape } from 'react-intl'

import styles from './styles.scss'

type Props = {
  className: ?string,
  name: string,
  first: ?boolean,
  maxWidth: ?boolean,
  intl: IntlShape,
  type?: string,
  onChange?: Function,
  messages: {
    label: MessageDescriptor,
  },
}

export class Input extends Component<Props, *> {
  static defaultProps = {
    type: 'text',
    onChange: () => {},
  }

  props: Props

  handleChange = (value: string, field: Object) => {
    // Call parent listener
    if (this.props.onChange) {
      this.props.onChange(value)
    }

    // Update redux-form value
    field.input.onChange(value)
  }

  renderField = (field: Object) => {
    const {
      className = '',
      first,
      messages,
      intl,
      type,
      onChange,
      name,
      maxWidth,
      ...rest
    } = this.props
    const { input, meta } = field

    // Use provided message or try to find label by field name
    const labelMessage = get(messages, 'label') || { id: `label.${name}` }
    const label = intl.formatMessage(labelMessage)
    const usePasswordClass = type === 'password' && input.value
    const error = meta.touched && meta.error && meta.error
    const classNameFinal = className || (first && 'mt-2')

    return (
      <RTInput
        theme={styles}
        className={cx(classNameFinal, {
          [styles.password]: usePasswordClass,
          [styles.maxWidth]: maxWidth,
        })}
        label={label}
        value={input.value}
        type={type}
        name={name}
        {...input}
        {...rest}
        error={error}
        onChange={e => this.handleChange(e, field)}
      />
    )
  }

  render() {
    const { name } = this.props

    return <Field name={name} component={this.renderField} />
  }
}

export default injectIntl(Input)

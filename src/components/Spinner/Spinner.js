// @flow

import React from 'react'
import { BSpinner } from '@prague-digi/ui-components'

import styles from './styles.scss'

type Props = {
  isVisible: boolean,
  messageId?: string,
}

const Spinner = ({ isVisible, messageId }: Props) => (
  <div className={styles.wrapper}>
    <BSpinner isShown={isVisible} isAbsolute message={messageId && { id: messageId }} />
  </div>
)

export default Spinner

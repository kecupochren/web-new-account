// @flow

import * as React from 'react'

import styles from './styles.scss'

type Props = {
  children: React.Node,
}

export const ButtonGroup = ({ children }: Props) => <div className={styles.wrapper}>{children}</div>

export default ButtonGroup

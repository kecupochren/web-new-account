// @flow
// React Router path definition
// Naming convention = constant-case page container names, e.g. containers/Home uses HOME
export default {
  INTRO: '/',
  SMS_VERIFICATION: '/sms-verification',
  ID_UPLOAD_FRONT: '/id-upload-front',
  ID_UPLOAD_BACK: '/id-upload-back',
  ID_MINED_DATA_CHECK: '/id-mined-data-check',
  SECOND_ID_SELECT: '/second-id-select',
  SECOND_ID_UPLOAD_FRONT: '/second-id-upload-front',
  SECOND_ID_MINED_DATA_CHECK: '/second-id-mined-data-check',
  SELFIE_UPLOAD: '/selfie-upload',
  DEVICE_TO_CONTINUE_ON_SELECT: '/device-to-continue-on-select',
  CONTINUE_ON_DESKTOP: '/continue-on-desktop',
  CARD_DELIVERY_ADDRESS: '/card-delivery-address',
  AML_QUESTIONS: '/aml-questions',
  PRE_CONTRACT_INFO: '/pre-contract-info',
  CONTRACT: '/contract',
  BANK_ACCOUNT_NUMBER: '/bank-account-number',
  BANK_ACCOUNT_VERIFICATION: '/bank-account-verification',
  VICTORY: '/victory',
}

// @flow

import { Errors } from '../constants/'
import type { ErrorMessage } from '../types/'

export type Status = {
  done: boolean,
  pending: boolean,
  error: ErrorMessage,
}

type Statuses = {
  DEFAULT: Status,
  PENDING: Status,
  SUCCESS: Status,
  FAILURE: Status,
  ERROR: (error: ErrorMessage) => Status,
}

const statuses: Statuses = {
  DEFAULT: { done: false, pending: false, error: null },
  PENDING: { done: false, pending: true, error: null },
  SUCCESS: { done: true, pending: false, error: null },
  FAILURE: { done: true, pending: false, error: Errors.DEFAULT },
  ERROR: (error: ErrorMessage = Errors.DEFAULT) => ({ done: true, pending: false, error }),
}

export default statuses

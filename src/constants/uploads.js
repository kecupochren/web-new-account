// @flow

export default {
  NATIONAL_ID: 'NATIONAL_ID',
  SECOND_ID: 'SECOND_ID',
  SELFIE: 'SELFIE',
}

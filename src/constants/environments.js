// @flow
// Definition of configurations by environment, such as API URL paths
// The environment key is set in src/index.js and handled in src/sagas/sagas.js
export default {
  MOCK: {
    apiUrl: 'http://localhost:3330',
  },
  DEV: {
    apiUrl: 'https://dmbs.internetbanka.cz/digdev1',
  },
}

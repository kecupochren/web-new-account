// @flow

// Used for Stepper
const WizardSections = {
  WELCOME: 'WELCOME',
  DOCUMENTS: 'DOCUMENTS',
  DETAILS: 'DETAILS',
  CONTRACT: 'CONTRACT',
  VERIFICATION: 'VERIFICATION',
}

export default WizardSections

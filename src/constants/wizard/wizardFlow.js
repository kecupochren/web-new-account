// @flow

import { Routes } from '../../constants/'
import type { WizardOptions } from '../../types/'

import { WizardSteps as Steps, WizardSections as Sections } from './index'

// prettier-ignore
const WizardFlow = {
  /**
   * Intro
   */
  [Steps.INTRO]: {
    url: Routes.INTRO,
    section: Sections.WELCOME,
    next: () => Steps.SMS_VERIFICATION,
    prev: () => '',
    canReturn: false,
  },

  /**
   * SMS Verification
   */
  [Steps.SMS_VERIFICATION]: {
    url: Routes.SMS_VERIFICATION,
    section: Sections.WELCOME,
    next: () => Steps.ID_UPLOAD_FRONT,
    prev: () => Steps.INTRO,
    canReturn: false,
  },

  /**
   * ID Upload
   */
  [Steps.ID_UPLOAD_FRONT]: {
    url: Routes.ID_UPLOAD_FRONT,
    section: Sections.DOCUMENTS,
    next: ({ isMobile }: WizardOptions) => isMobile ? Steps.ID_UPLOAD_BACK : Steps.CARD_DELIVERY_ADDRESS,
    prev: () => Steps.SMS_VERIFICATION,
    canReturn: true,
  },

  [Steps.ID_UPLOAD_BACK]: {
    url: Routes.ID_UPLOAD_BACK,
    section: Sections.DOCUMENTS,
    next: () => Steps.ID_MINED_DATA_CHECK,
    prev: () => Steps.ID_UPLOAD_FRONT,
    canReturn: true,
  },

  [Steps.ID_MINED_DATA_CHECK]: {
    url: Routes.ID_MINED_DATA_CHECK,
    section: Sections.DOCUMENTS,
    next: () => Steps.SECOND_ID_SELECT,
    prev: () => Steps.ID_UPLOAD_BACK,
    canReturn: true,
  },

  /**
   * Second ID Upload
   */
  [Steps.SECOND_ID_SELECT]: {
    url: Routes.SECOND_ID_SELECT,
    section: Sections.DOCUMENTS,
    next: () => Steps.SECOND_ID_UPLOAD_FRONT,
    prev: () => Steps.ID_MINED_DATA_CHECK,
    canReturn: false,
  },

  [Steps.SECOND_ID_UPLOAD_FRONT]: {
    url: Routes.SECOND_ID_UPLOAD_FRONT,
    section: Sections.DOCUMENTS,
    next: () => Steps.SECOND_ID_MINED_DATA_CHECK,
    prev: () => Steps.SECOND_ID_SELECT,
    canReturn: true,
  },

  [Steps.SECOND_ID_MINED_DATA_CHECK]: {
    url: Routes.SECOND_ID_MINED_DATA_CHECK,
    section: Sections.DOCUMENTS,
    next: ({ isMobile }: WizardOptions) => isMobile ? Steps.SELFIE_UPLOAD : Steps.SELFIE_UPLOAD,
    prev: () => Steps.SECOND_ID_UPLOAD_FRONT,
    canReturn: true,
  },

  /**
   * Selfie upload
   */
  [Steps.SELFIE_UPLOAD]: {
    url: Routes.SELFIE_UPLOAD,
    section: Sections.DOCUMENTS,
    next: ({ isMobile }: WizardOptions) => isMobile ? Steps.DEVICE_TO_CONTINUE_ON_SELECT : Steps.CARD_DELIVERY_ADDRESS,
    prev: () => Steps.SECOND_ID_MINED_DATA_CHECK,
    canReturn: true,
  },

  /**
   * Documents uploaded
   */
  [Steps.DEVICE_TO_CONTINUE_ON_SELECT]: {
    url: Routes.DEVICE_TO_CONTINUE_ON_SELECT,
    section: Sections.DOCUMENTS,
    next: ({ continueOnMobile }: WizardOptions) => continueOnMobile ? Steps.CARD_DELIVERY_ADDRESS : Steps.CONTINUE_ON_DESKTOP,
    prev: () => Steps.SELFIE_UPLOAD,
    canReturn: true,
  },

  [Steps.CONTINUE_ON_DESKTOP]: {
    url: Routes.CONTINUE_ON_DESKTOP,
    section: Sections.DOCUMENTS,
    next: () => '',
    prev: () => Steps.DEVICE_TO_CONTINUE_ON_SELECT,
    canReturn: true
  },

  /**
   * Details
   */
  [Steps.CARD_DELIVERY_ADDRESS]: {
    url: Routes.CARD_DELIVERY_ADDRESS,
    section: Sections.DETAILS,
    next: () => Steps.AML_QUESTIONS,
    prev: ({ isMobile }: WizardOptions) => isMobile ? Steps.SELFIE_UPLOAD : Steps.ID_UPLOAD_FRONT,
    canReturn: true,
  },

  [Steps.AML_QUESTIONS]: {
    url: Routes.AML_QUESTIONS,
    section: Sections.DETAILS,
    next: () => Steps.PRE_CONTRACT_INFO,
    prev: () => Steps.CARD_DELIVERY_ADDRESS,
    canReturn: true,
  },

  /**
   * Contract
   */
  [Steps.PRE_CONTRACT_INFO]: {
    url: Routes.PRE_CONTRACT_INFO,
    section: Sections.CONTRACT,
    next: () => Steps.CONTRACT,
    prev: () => Steps.AML_QUESTIONS,
    canReturn: true,
  },

  [Steps.CONTRACT]: {
    url: Routes.CONTRACT,
    section: Sections.CONTRACT,
    next: () => Steps.BANK_ACCOUNT_NUMBER,
    prev: () => Steps.PRE_CONTRACT_INFO,
    canReturn: true,
  },

  /**
   * Bank Account Verification
   */
  [Steps.BANK_ACCOUNT_NUMBER]: {
    url: Routes.BANK_ACCOUNT_NUMBER,
    section: Sections.VERIFICATION,
    next: () => Steps.BANK_ACCOUNT_VERIFICATION,
    prev: () => Steps.CONTRACT,
    canReturn: true,
  },

  [Steps.BANK_ACCOUNT_VERIFICATION]: {
    url: Routes.BANK_ACCOUNT_VERIFICATION,
    section: Sections.VERIFICATION,
    next: () => Steps.VICTORY,
    prev: () => Steps.BANK_ACCOUNT_NUMBER,
    canReturn: true,
  },

  /**
   * Victory
   */
  [Steps.VICTORY]: {
    url: Routes.VICTORY,
    section: Sections.VERIFICATION,
    next: () => '',
    prev: () => Steps.BANK_ACCOUNT_VERIFICATION,
    canReturn: true,
  },
}

export default WizardFlow

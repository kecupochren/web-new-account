// @flow

export default {
  // key: intl message
  DEFAULT: {
    id: 'error.default',
    defaultMessage: 'Unknown technical failure',
  },
}

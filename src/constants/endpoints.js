// @flow
// Common API endpoint definition
// Naming convention = constant-case API method names, e.g. api.getFoo uses GET_FOO constant
export default {
  GET_GREETING: '/greeting',
  CREATE_CUSTOMER: '/caapi/unsecured/acquisition/customer',
  SCAN_DOCUMENT: '/caapi/unsecured/personal/document/scan',
  FETCH_VALIDATIONS: '/json/ca-onboarding/all_cs.json',
}

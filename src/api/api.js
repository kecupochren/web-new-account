// @flow
import axios from 'axios'

import { Endpoints } from '../constants'
import type { ConfigState, Api } from '../types'

const saveProgressMock = application => {
  window.localStorage.setItem('moneta-onboarding', JSON.stringify(application))

  return Promise.resolve({})
}

const loadProgressMock = () => {
  const progress = window.localStorage.getItem('moneta-onboarding')

  return Promise.resolve(JSON.parse(progress))
}

const createApi = (config: ConfigState): Api => {
  const api = axios.create({ baseURL: config.apiUrl, withCredentials: true })

  return {
    getGreeting: () => api.get(Endpoints.GET_GREETING),

    getValidations: () => api.get(Endpoints.FETCH_VALIDATIONS),
    createCustomer: (data: Object) => api.post(Endpoints.CREATE_CUSTOMER, data),
    scanDocument: (data: Object) => api.post(Endpoints.SCAN_DOCUMENT, data),

    saveProgress: saveProgressMock,
    loadProgress: loadProgressMock,
  }
}

export default createApi

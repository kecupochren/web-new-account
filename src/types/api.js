// @flow
// All API related custom types should be located here: request and response shapes, etc.
export type Api = {
  getGreeting: () => mixed,
  createCustomer: (data: Object) => mixed,
  scanDocument: (data: Object) => mixed,
  getValidations: () => mixed,
  saveProgress: () => mixed,
  loadProgress: () => mixed,
}

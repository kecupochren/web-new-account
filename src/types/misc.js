// @flow

import type { MessageDescriptor } from 'react-intl'

export type ErrorMessage = MessageDescriptor | string | null

export type IDScanDocument = {
  contentType: string,
  data: string | ArrayBuffer,
  documentType: string,
  originalFilename: string,
  page: string,
}

export type IDScanDocuments = {
  front: ?IDScanDocument,
  back: ?IDScanDocument,
}

/**
 * Expose internals
 */

/** Containers */
export type { WizardRenderProps, WizardActionsFinal } from '../containers/Wizard'
export type { UploadedDocument } from '../containers/DocumentUpload/DocumentUpload'

/** Constants */
export type { Status } from '../constants/statuses'

/** Utils */
export type { Validations } from '../utils/validation/createValidations'

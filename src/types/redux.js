// @flow

import type { Saga as SagaType } from 'redux-saga'
import type { AppState } from '../redux/app'
import type { AuthState, AuthActions } from '../redux/auth'
import type { WizardState, WizardActions } from '../redux/wizard'
import type { DocumentsState } from '../redux/documents'
import type { DevMenuState } from '../redux/devMenu'

// Reexport internals
export * from '../redux/app'
export * from '../redux/wizard/model'
export type { DevMenuState } from '../redux/devMenu'
export { WizardActions } from '../redux/wizard/wizard'
export { DocumentsActions } from '../redux/documents'

// General Redux types
export type Action = {
  type: string,
  payload: any,
  meta?: any,
}

export type Dispatch = (action: Action) => void

export type Saga = SagaType<*>

// State shapes
export type ConfigState = {
  apiUrl: string,
  environment: string,
  lang: string,
}

export type CustomerState = {
  greeting: string,
  name: string,
}

export type State = {
  app: AppState,
  config: ConfigState,
  devMenu: DevMenuState,
  customer: CustomerState,
  documents: DocumentsState,
  form: any,
  auth: AuthState,
  wizard: WizardState,
}

export type Actions = WizardActions & AuthActions & any

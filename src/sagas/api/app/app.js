// @flow

import { call, put } from 'redux-saga/effects'

import { appActions } from '../../../redux/app'
import { getResponseData } from '../../../utils/'
import type { Api, Saga } from '../../../types/'

export function* getValidationsSaga(api: Api): Saga {
  const res = yield call(api.getValidations)
  const data = getResponseData(res)
  const validations = { rules: data.validations, errorMessages: data.errorMessages }

  yield put(appActions.putValidations(validations))
}

export function* initializeSaga(api: Api): Saga {
  try {
    yield call(getValidationsSaga, api)
    yield put(appActions.initializeSuccess())
  } catch (error) {
    console.error(error) // eslint-disable-line
    yield put(appActions.initializeFailure())
  }
}

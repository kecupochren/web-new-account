// @flow

import { all, put, call, select } from 'redux-saga/effects'
import { change } from 'redux-form'
import get from 'lodash.get'

// import { api } from '../../../api/'
import { createCustomerPayload, getDocuments, getScannedData } from '../../../redux/selectors'
import { authActions } from '../../../redux/auth'
import type { Saga, Api } from '../../../types/'

import { normalizeData, isScanDataValid } from './auth.utils'

export function* createCustomerSaga(api: Api): Saga {
  const payload = yield select(createCustomerPayload)

  try {
    const res = yield call(api.createCustomer, payload)

    if (res.status === 200) {
      yield put(authActions.createCustomerSuccess())
    } else {
      yield put(authActions.createCustomerFailure())
    }
  } catch (error) {
    console.error(error) // eslint-disable-line
  }
}

/**
 * TODO: Move to a different module (documents?)
 */
export function* mineDataFromScanSaga(api: Api, document: Object): ?Object {
  try {
    const payload = {
      contentType: document.contentType,
      data: document.data,
      documentType: document.documentType,
      originalFilename: document.originalFilename,
      page: document.page,
    }
    const { data, status } = yield call(api.scanDocument, payload)

    if (status === 200) {
      return yield get(data, 'responseObject.MinedModel')
    }
  } catch (error) {
    console.error(error) // eslint-disable-line
  }

  return null
}

export function* getScannedDataSaga(api: Api): Saga {
  const documents = yield select(getDocuments)

  const [front, back] = yield all([
    yield call(mineDataFromScanSaga, api, documents.front),
    yield call(mineDataFromScanSaga, api, documents.back),
  ])

  return { front, back }
}

export function* processScannedDataSaga(scannedData: Object): ?Object {
  if (!isScanDataValid(scannedData)) {
    // TODO: Handle err
    yield put(authActions.scanDocumentsFailure())
  }

  return normalizeData(scannedData)
}

export function* scanDocumentsSaga(api: Api): Saga {
  try {
    const scannedData = yield call(getScannedDataSaga, api)
    const processedData = yield call(processScannedDataSaga, scannedData)

    yield put(authActions.scanDocumentsSuccess(processedData))
  } catch (error) {
    console.error(error) // eslint-disable-line
    yield put(authActions.scanDocumentsFailure())
  }
}

export function* applyScannedDataSaga(): Saga {
  const data = yield select(getScannedData)

  yield all(
    Object.keys(data).map(field => {
      const value = data[field]

      return value && put(change('wizard', field, value))
    })
  )
}

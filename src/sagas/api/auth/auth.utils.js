// @flow

import get from 'lodash.get'
import capitalize from 'lodash.capitalize'

import { DocumentCodes } from '../../../constants/'

type ScannedData = {
  front: Object,
  back: Object,
  code: string,
}

// --- HELPERS ---------------------------------------

const getName = (data: ScannedData) => ({
  firstName: capitalize(get(data.front, 'FirstName.Text')),
  lastName: capitalize(get(data.front, 'LastName.Text')),
})

const getZip = (addressText: string) =>
  typeof addressText === 'string' &&
  addressText
    .replace(/ /g, '') // crop all spaces
    .split(',') // split string - comma
    .slice(-1)[0] // take last item of array
    .slice(0, 5) // take first five characters

const getIdCardData = (data: ScannedData) => {
  let idCardValidTo = get(data.front, 'ExpiryDate.Text')

  // if string then remove spaces from the date
  if (typeof idCardValidTo === 'string') {
    idCardValidTo = idCardValidTo.replace(/ /g, '')
  }

  return {
    idCardValidTo,
    idCardNumber: get(data.front, 'IdcardNumber.Text'),
  }
}

const getAddress = (data: ScannedData) => {
  const address = {
    street: capitalize(get(data.back, 'Address.A2')),
    zip: getZip(get(data.back, 'Address.Text')),
  }

  if (DocumentCodes.IDC2 === data.code) {
    return {
      ...address,
      // The first address line is in form [City, region] => get City only
      city: capitalize(get(data.back, 'Address.A1', '').split(',')[0]),
    }
  }

  return {
    ...address,
    city: capitalize(get(data.back, 'Address.A3')),
  }
}

const getBirthAddress = (data: ScannedData) => {
  // Check on which side the data appears - based on id type
  const page = DocumentCodes.IDC2 === data.code ? data.front : data.back

  return {
    // The first birth address line is birth city => get it
    birthPlace: capitalize(get(page, 'BirthAddress.Text', '').split('\n')[0]),
  }
}

const getBirthNumber = (data: ScannedData) => {
  // Check on which side the data appears - based on id type
  const page = DocumentCodes.IDC2 === data.code ? data.back : data.front

  return {
    birthNumber: get(page, 'BirthNumber.Text'),
  }
}

// --- EXPORTS ---------------------------------------

export function getDocumentCodes(scannedData: Object) {
  const front = get(scannedData.front, 'DocumentCode', '')
  const back = get(scannedData.back, 'DocumentCode', '')

  return { front, back }
}

export function isScanDataValid(scannedData: Object): boolean {
  const { front, back } = getDocumentCodes(scannedData)

  return Boolean(front !== back || !DocumentCodes[front] || !DocumentCodes[back])
}

export function normalizeData(scannedData: Object) {
  const data = {
    ...scannedData,
    code: getDocumentCodes(scannedData).front,
  }

  const normalizedData = {
    ...getName(data),
    ...getIdCardData(data),
    ...getAddress(data),
    ...getBirthAddress(data),
    ...getBirthNumber(data),

    nationality: get(data.back, 'Nationality.Text'),
  }

  return normalizedData
}

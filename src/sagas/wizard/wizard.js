// @flow

import { all, put, call, select } from 'redux-saga/effects'
import { change } from 'redux-form'
import { push } from 'react-router-redux'
import * as Scroll from 'react-scroll'

import { WizardSteps, WizardFlow } from '../../constants'
import {
  getNextWizardStep,
  getApiEnabled,
  getCurrentWizardStepName,
  getFormValues,
  getSecondIdDocument,
  getDocumentsState,
} from '../../redux/selectors'
import { wizardActions } from '../../redux/wizard'
import { documentsActions } from '../../redux/documents'
import { scanDocumentsSaga, createCustomerSaga, applyScannedDataSaga } from '../api/auth'
import type { Action, Saga, Api } from '../../types/'

export function* loadProgressSaga(api: Api): Saga {
  const progress = yield call(api.loadProgress)
  const useProgress = JSON.parse(window.localStorage.getItem('enablePersistentState'))

  if (progress && useProgress) {
    const { step, data, documents } = progress

    // Set active step
    yield put(wizardActions.setActiveStep(step))
    yield put(push(WizardFlow[step].url))

    // Load form data
    yield all(
      Object.keys(data).map(field => data[field] && put(change('wizard', field, data[field])))
    )

    // Load docs state
    if (documents) {
      yield put(documentsActions.loadState(documents))
    }

    yield put(wizardActions.loadProgressSuccess())
  } else {
    yield put(push('/'))
  }
}

export function* saveProgressSaga(api: Api): Saga {
  const step = yield select(getCurrentWizardStepName)
  const data = yield select(getFormValues)
  const secondIDDocument = yield select(getSecondIdDocument)
  const documents = yield select(getDocumentsState)

  const progress = { step, data, secondIDDocument, documents }

  yield call(api.saveProgress, progress)
}

export function* handleSuccessSaga(step: string): Saga {
  const nextStep = yield select(getNextWizardStep)

  yield put(wizardActions.handleSubmitStepSuccess(step))
  yield put(wizardActions.setActiveStep(nextStep.name))
  yield put(push(nextStep.url))

  Scroll.animateScroll.scrollTo(0, { duration: 400 })
}

// We call this single saga from wizard UI to further separate concerns...
export function* handleWizardSubmitSaga(api: Api, action: Action): Saga {
  const apiEnabled = yield select(getApiEnabled)
  const step = action.payload

  // Execute step specific logic
  if (apiEnabled) {
    switch (step) {
      case WizardSteps.INTRO: {
        yield call(createCustomerSaga, api)
        break
      }

      case WizardSteps.ID_UPLOAD_BACK: {
        yield call(scanDocumentsSaga, api)
        yield call(applyScannedDataSaga, api)
        break
      }

      default:
        break
    }
  }

  // Continue flow
  yield call(handleSuccessSaga, step)

  // Save application progress
  yield call(saveProgressSaga, api)
}

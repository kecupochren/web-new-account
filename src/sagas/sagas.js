// @flow
import { all, cancel, fork, select, take, takeLatest } from 'redux-saga/effects'
// $FlowFixMe
import type { IOEffect } from 'redux-saga/effects'

import createApi from '../api'
import { appActions } from '../redux/app'
import { authActions } from '../redux/auth'
import { configActions } from '../redux/config'
import { wizardActions } from '../redux/wizard'
import { getConfig } from '../redux/selectors'
import type { Saga } from '../types/'

import { initializeSaga } from './api/app'
import { createCustomerSaga, scanDocumentsSaga } from './api/auth'
import { handleWizardSubmitSaga, loadProgressSaga } from './wizard'

// action type which cancels a running rootSaga in cancellableSaga
const CANCEL_ROOT_SAGA = 'CANCEL_ROOT_SAGA'

export function* createApiAndSagas(): Saga {
  const config = yield select(getConfig)
  const api = createApi(config)

  yield all([
    takeLatest(appActions.initialize().type, initializeSaga, api),

    // $FlowFixMe
    takeLatest(wizardActions.handleSubmitStep().type, handleWizardSubmitSaga, api),
    takeLatest(wizardActions.loadProgress().type, loadProgressSaga, api),
    takeLatest(authActions.createCustomer().type, createCustomerSaga, api),
    takeLatest(authActions.scanDocuments().type, scanDocumentsSaga, api),
  ])
}

// business logic saga entry point
export function* rootSaga(): Generator<IOEffect, void, Object> {
  yield takeLatest(configActions.setEnvironment().type, createApiAndSagas)
}

// this saga is to be run by sagaMiddleware in order for HMR to work
// note that when saga HMR is enabled, changes in src/redux will also trigger the cancellation
export function* cancellableSaga(environment: string): Generator<IOEffect, void, Object> {
  // start the root saga
  const task = yield fork(rootSaga, environment)

  // cancelling mechanism
  yield take(CANCEL_ROOT_SAGA)
  yield cancel(task)
}

// default export, you should start and cancel the rootSaga via resulting sagaManager exclusively
export default function createSagaManager(store: Object) {
  return {
    cancel: () => store.dispatch({ type: CANCEL_ROOT_SAGA }),
    start: (environment: string) => store.sagaMiddleware.run(cancellableSaga, environment),
  }
}

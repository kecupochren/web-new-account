const status = 200
const headers = require('../../../../cors-json.js')

const body = {
  status: 'OK',
  responseObject: { validation: 'bar' },
}

module.exports = {
  status,
  body,
  headers,
}

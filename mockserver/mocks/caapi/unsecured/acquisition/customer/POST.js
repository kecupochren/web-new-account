const status = 200
const headers = require('../../../../../cors-json.js')

const body = {
  status: 'OK',
}

module.exports = {
  status,
  body,
  headers,
}

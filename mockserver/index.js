const http = require('http')
const mockserver = require('@prague-digi/mockserver')

const port = 3330

const server = http.createServer((req, res) => {
  if (req.method === 'OPTIONS') {
    res.setHeader('Access-Control-Request-Method', '*')
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000')
    res.setHeader('Access-Control-Allow-Credentials', true)
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT')
    res.setHeader('Access-Control-Allow-Headers', req.headers['access-control-request-headers'])
    res.writeHead(200)
    res.end()

    return
  }

  mockserver('mockserver/mocks').call(null, req, res)
})

server.listen(port)
